class Match{
	constructor(build){
		this.country = build.country;
		this.tournament = build.tournament;
		this.timer = build.timer;
		this.team_home = build.team_home;
		this.team_away = build.team_away;
		this.score = build.score;
		this.time = build.time;
	}
	
	toString(){
		return 
			"Country " + this.country + ", " + 
			"Tournament " + this.tournament + ", " + 
			"Timer " + this.timer + ", " + 
			"Team_home " + this.team_home + ", " + 
			"Team_away " + this.team_away + ", " + 
			"Score " + this.score + ", " + 
			"Time" + this.time;
	}
	
	static get Builder() {
		class Builder {
			constructor() {}
			setCountry(country) {this.country = country; return this;}
			setTournament(tournament) {this.tournament = tournament; return this;}
			setTimer(timer) {this.timer = timer; return this;}
			setTeamHome(team_home) {this.team_home = team_home; return this;}
			setTeamAway(team_away) {this.team_away = team_away; return this;}
			setScore(score) {this.score = score; return this;}
			setTime(time) {this.time = time; return this;}
			build() {return new Match(this);}
		}
		return Builder;
	}
}

function getFormattedDate(date, separator = "") {
	return "" + 
		date.getFullYear() + separator + 
		("0" + (date.getMonth()+1)).slice(-2) + separator + 
		("0" + date.getDate()).slice(-2);
}

function go_to_date(driver, currentdate, nbr_days) {
	var selenium = require('selenium-webdriver');
	var By = selenium.By;
	var calendar = driver.findElement(By.css('.calendar__datepicker'));
	
	calendar.click();
	
	var date = calendar.findElement(By.css('.calendar__datepicker--dates .day:nth-child(' + (8 + nbr_days) + ')'));
	date.click();
	
	currentdate.setDate(currentdate.getDate() + nbr_days);

	return currentdate;
}

function check_date(driver, currentdate, stompClient) {
	var selenium = require('selenium-webdriver');
	var By = selenium.By;
	
	return driver.findElement(By.css('#ifmenu-calendar span.today')).getText().then(function(date) {
		print(currentdate + " " + date, 3);
		date = date.split(/[^0-9]/);
		if (currentdate.getDate() != date[0]
		|| currentdate.getMonth() + 1 != date[1]) {
			print("Invalid date", 1);
			driver.quit();
			disconnectWebSocket(stompClient);
			throw "Invalid date";
		}
	});
}

function print(msg, debug_level) {
	if (typeof debug_level == 'undefined'
				|| [1, 3].indexOf(debug_level) == -1) {
		debug_level = 1;
	}
	
	if (debug_level & DEBUG_LEVEL >= debug_level) {
		console.log(msg);
	}
}

function wait_loading_matches(driver) {
	var selenium = require('selenium-webdriver');
	var By = selenium.By;
	
	driver.then(function(){
		driver.findElements(By.css('.event__match')).then(function(els1){
			return els1.length;
		}).then(function(els1_length){
			driver.sleep(100);
			driver.findElements(By.css('.event__match')).then(function(els2) {
				print("Number of tables: " + els1_length + " and " + els2.length, 3);
				if (els1_length != els2.length) {
					return wait_loading_matches(driver);
				}
			});
		});
	});
}

function disconnectWebSocket(stompClient) {
	if(stompClient != null) {
		stompClient.disconnect();
	}
	print("Disconnected", 3);
}

function create_match(unplayed_match, dom_match, country_tournament, currentdate) {
	var separator_index = country_tournament.indexOf(":");
	var country, tournament, timer, team_home, team_away, score, time;
	
	country = tournament = timer = team_home = team_away = score = time = "";
	
	country = country_tournament.substring(0, separator_index).trim();
	var tournament = country_tournament.substring(separator_index + 1).trim();
	team_home = dom_match.find(".event__participant--home").text().trim();
	team_away = dom_match.find(".event__participant--away").text().trim();
	score = dom_match.find(".event__scores").text().trim();
	
	if (unplayed_match == false) {
		time = getFormattedDate(currentdate, "/");
		timer = dom_match.find(".event__stage").text().trim();
	} else {
		time = getFormattedDate(currentdate, "/") + " " + dom_match.find(".event__time").text().trim();
	}
	
	match = new Match.Builder()
					.setCountry(country)
					.setTournament(tournament)
					.setTimer(timer)
					.setTeamHome(team_home)
					.setTeamAway(team_away)
					.setScore(score)
					.setTime(time)
					.build();
	
	return match;
}

function create_json_file(raw_filename, filename, currentdate, unplayed_match) {
	var htmlparser = require("htmlparser2");
	const cheerio = require('cheerio');
	var fs = require('fs');
	var domString = fs.readFileSync(raw_filename, 'utf8');
	var dom = htmlparser.parseDOM(domString);
	
	var matches = new Array();
	const $ = cheerio.load(domString);
	const value = $("html").find("");
	
	var dom_date = $("html").find(".calendar__datepicker").text();
	dom_date = dom_date.split(/[^0-9]/);
	
	if (currentdate.getDate() != dom_date[0]
			|| currentdate.getMonth() + 1 != dom_date[1]) {
		throw "Invalid date";
	}
	
	$('.event__header').each(function(i, elem) {
		
		country_tournament = $(this).find('.event__title').text();
		
		var dom_match = $(this).next(".event__match");
		
		while (dom_match.hasClass("event__match") == true) {
			match = create_match(unplayed_match, dom_match, country_tournament, currentdate);
			matches.push(match);
			
			dom_match = dom_match.next(".event__match");
		}
	});
	
	fs.writeFile(filename, JSON.stringify(matches, null, 2), function(err) {
		if(err) {
			return print(err, 3);
		}
		
		console.log("The file: " + filename + " was saved!");
	});
}

function send_progress(stompClient, notificationId, cvalue, fvalue) {
	stompClient.send("/topic/messages/progress", {}, 
		      JSON.stringify({notificationId: notificationId, progress: parseInt(cvalue*100/fvalue) + "%"}));
	print("Progress: " + cvalue + " / " + fvalue, 3);
}

module.exports.getFormattedDate = getFormattedDate;
module.exports.go_to_date = go_to_date;
module.exports.check_date = check_date;
module.exports.print = print;
module.exports.wait_loading_matches = wait_loading_matches;
module.exports.disconnectWebSocket = disconnectWebSocket;
module.exports.create_match = create_match;
module.exports.create_json_file = create_json_file;
module.exports.send_progress = send_progress;
	
// 0 = no debug messages
// 1 = default debug messages
// 3 = verbose debug messages
const DEBUG_LEVEL = 3;
