var Utils = require('./utils');

require('chromedriver');
var htmlparser = require("htmlparser2");
const cheerio = require('cheerio');

var fs = require('fs');
var selenium = require('selenium-webdriver');
var SockJS = require('sockjs-client');
var Stomp = require('stompjs');
var socket = new SockJS('http://127.0.0.1:8080/scoremining/notifications');
var stompClient = Stomp.over(socket);
var error_flag = 0;

const driver = new selenium.Builder().forBrowser('chrome').build();

var currentdate = new Date();
var unplayed_match;
var path_raw = "C:\\eclipse\\workspace\\scoremining\\crawler\\matches_files\\raw\\";
var path = "C:\\eclipse\\workspace\\scoremining\\crawler\\matches_files\\unsavedtodb\\";

stompClient.connect({}, function(frame) {
	Utils.print('Connected: ' + frame, 3);
});

driver.get('http://www.flashscore.com/');

nr_days = parseInt(process.argv[2]);
notificationId = parseInt(process.argv[3]);

//Utils.send_progress(stompClient, notificationId, 1, 100);

unplayed_match = nr_days < 0 ? false : true;
currentdate = Utils.go_to_date(driver, currentdate, nr_days);
//Utils.send_progress(stompClient, notificationId, 5, 100);

Utils.wait_loading_matches(driver);
//Utils.send_progress(stompClient, notificationId, 10, 100);

driver.findElement({
	css : 'html'
}).getAttribute("innerHTML").then(
		function(e) {
			var filename = (unplayed_match ? "u" : "m")
					+ Utils.getFormattedDate(currentdate);
			fs.writeFile(path_raw + filename, e,
					function(err) {
						if (err) {
							Utils.disconnectWebSocket(stompClient);
							return Utils.print(err, 3);
						}

						//Utils.send_progress(stompClient, notificationId, 100,
							//	100);

						console.log("The file: " + path_raw + filename
								+ " was saved!");

						Utils.create_json_file(path_raw + filename, path
								+ filename, currentdate, unplayed_match);
					});
		});

driver.quit();
