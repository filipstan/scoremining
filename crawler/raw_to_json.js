/*
 * 
 * $ node raw_to_json.js 11.03 m20190311 11 3
 * The file: m20190311 was saved!
 *
 */

var Utils = require('./utils');

var currentdate = new Date();

currentdate.setDate(process.argv[4]);
currentdate.setMonth(parseInt(process.argv[5]) - 1);

Utils.create_json_file(process.argv[2], process.argv[3], currentdate, false);
