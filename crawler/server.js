var express = require('express');
var bodyParser = require('body-parser');
const fs = require('fs');

var app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: true
})); 

app.get('/callCrawler', function(req, res) {
	
	var days = parseInt(req.query.nbrDays);
	var notificationId = parseInt(req.query.notificationId);
	var ret;

	ret = runScript('node ./crawler.js ' + days + " " + notificationId);
	
	res.json({msg: ret});
});

app.post('/scm_send_data', function(req, res){
	var ip = req.body.ip;
	var now = new Date();
	now = getFormattedDateTime(now);
	var data = "[" + now + "] " + ip + "\n"
	
	console.log(data);
	fs.appendFileSync('scm_ip.txt', data);
	
	res.end();
});

app.listen(3000);

function getFormattedDateTime(date){
	return "" + 
		date.getFullYear() + "/" + 
		("0" + (date.getMonth()+1)).slice(-2) + "/" + 
		("0" + date.getDate()).slice(-2) + " " +
		("0" + date.getHours()).slice(-2) + ":" +
		("0" + date.getMinutes()).slice(-2) + ":" +
		("0" + date.getMinutes()).slice(-2);
}

function runScript(scriptPath) {
	var childProcess = require('child_process');
	return childProcess.execSync(scriptPath, {timeout: 0}).toString();
}