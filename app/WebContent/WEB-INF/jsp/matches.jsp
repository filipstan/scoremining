<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Admin - Matches</title>

<!-- Bootstrap Core CSS -->
<link
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<c:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- DataTables CSS -->
<link
	href="<c:url value="/resources/vendor/datatables-plugins/dataTables.bootstrap.css"/>"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link
	href="<c:url value="/resources/vendor/datatables-responsive/dataTables.responsive.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="/resources/dist/css/sb-admin-2.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/dist/css/admin.css"/>"
	rel="stylesheet">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<!-- Custom Fonts -->
<link
	href="<c:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="wrapper">
		<jsp:include page="sidebar.jsp"></jsp:include>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Matches</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseOne">Match played files</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6">
										<form role="form" name="saveFileMpToDbForm"
											id="saveFileMpToDbForm" method="POST"
											action='<c:url value="/admin/saveFileMpToDb"/>'>
											<div class="form-group">
												<label>Match played files</label> <select multiple
													name="filenames" class="form-control">
												</select>
											</div>
											<button id="saveFileMpToDbBtn" type="submit"
												class="btn btn-default">
												Save <i class="hidden fa fa-gear fa-spin"></i>
											</button>
											<button id="listMpFilesBtn" type="button"
												class="btn btn-default">
												Refresh <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</form>
									</div>
									<div class="col-lg-6">
										<form role="form" name="callCrawlerForm" id="callCrawlerForm"
											method="POST" action='<c:url value="/admin/callCrawler"/>'>
											<div class="form-group">
												<label>Date</label> <input id="callCrawlerDate"
													name="callCrawlerDate" class="form-control" />
											</div>
											<button id="callCrawlerBtn" type="submit"
												class="btn btn-outline btn-default btn-lg btn-block">
												Call crawler <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseTwo">Match unplayed files</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-6">
										<form role="form" name="saveFileMuToDbForm"
											id="saveFileMuToDbForm" method="POST"
											action='<c:url value="/admin/saveFileMuToDb"/>'>
											<div class="form-group">
												<label>Match unplayed files</label> <select multiple
													name="filenames" class="form-control">
												</select>
											</div>
											<button id="saveFileMuToDbBtn" type="submit"
												class="btn btn-default">
												Save <i class="hidden fa fa-gear fa-spin"></i>
											</button>
											<button id="listMuFilesBtn" type="button"
												class="btn btn-default">
												Refresh <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</form>
									</div>
									<div class="col-lg-6">
										<form role="form" name="callCrawlerForm2"
											id="callCrawlerForm2" method="POST"
											action='<c:url value="/admin/callCrawler"/>'>
											<div class="form-group">
												<label>Date</label> <input id="callCrawlerDate2"
													name="callCrawlerDate" class="form-control" />
											</div>
											<button id="callCrawlerBtn2" type="submit"
												class="btn btn-outline btn-default btn-lg btn-block">
												Call crawler <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Matches Played</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-matches-played">
								<thead>
									<tr>
										<th>Id</th>
										<th>Home team</th>
										<th>Away team</th>
										<th>Score</th>
										<th>Competition</th>
										<th>Country</th>
										<th>Date</th>
									</tr>
								</thead>
							</table>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Matches Unplayed</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-matches-unplayed">
								<thead>
									<tr>
										<th>Id</th>
										<th>Home team</th>
										<th>Away team</th>
										<th>Competition</th>
										<th>Country</th>
										<th>Date</th>
									</tr>
								</thead>
							</table>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="<c:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<c:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>

	<!-- DataTables JavaScript -->
	<script
		src="<c:url value="/resources/vendor/datatables/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/datatables-responsive/dataTables.responsive.js"/>"></script>

	<!-- SockJs JavaScript -->
	<script
		src="<c:url value="/resources/vendor/sockjs/sockjs.js"/>"></script>
	<!-- STOMP JavaScript -->
	<script
		src="<c:url value="/resources/vendor/stomp/stomp.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<jsp:include page="admin_js.jsp"></jsp:include>
	<script src="<c:url value="/resources/dist/js/sb-admin-2.js"/>"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
	var tableMatchesPlayed, tableMatchesUnplayed;
	$(document)
			.ready(
					function() {
						tableMatchesPlayed = $('#dataTables-matches-played')
								.DataTable(
										{
											ajax: {
									            "url": '<c:url value="/admin/getMatchesPlayed/"/>',
									            "type": "POST"
									        },
											responsive : true,
											"deferRender": true,
											"dom" : '<"toolbar">frtip',
											order: [[6, "desc"]]
										});
						tableMatchesUnplayed = $(
								'#dataTables-matches-unplayed')
								.DataTable(
										{
											ajax: {
									            "url": '<c:url value="/admin/getMatchesUnplayed/"/>',
									            "type": "POST"
									        },
											responsive : true,
											"deferRender": true,
											"dom" : '<"toolbar">frtip',
											order: [[5, "desc"]]
										});

	                	var formGroupDiv = $(
							'<div/>',
							{
								class : 'form-group margin-right-10'
							});
	                
						var fromDateInput = formGroupDiv.clone().append($(
								'<input/>',
								{
									id : 'fromDatepickerMp',
									name : 'fromDate',
									class : 'form-control',
									type : 'text',
									placeholder : 'From'
								}));

						var toDateInput = formGroupDiv.clone().append($(
								'<input/>',
								{
									id : 'toDatepickerMp',
									name : 'toDate',
									class : 'form-control',
									type : 'text',
									placeholder : 'To'
								}));
						
						var buttonGetMatchesPlayed = $(
								'<button/>',
								{
									id : 'buttonGetMatchesPlayed',
									text : 'Get matches',
									class : 'btn btn-default margin-right-10', 
									click : function() {
										tableMatchesPlayed.ajax.url('<c:url value="/admin/getMatchesPlayed/?fromDate=' + $('#fromDatepickerMp').val() + '&toDate=' + $('#toDatepickerMp').val() + '"/>').load();
									}
								});

						$("#dataTables-matches-played_wrapper div.toolbar").append(fromDateInput, toDateInput, buttonGetMatchesPlayed);

						$( "#fromDatepickerMp" ).datepicker({ dateFormat: 'yy-mm-dd' });
						$( "#toDatepickerMp" ).datepicker({ dateFormat: 'yy-mm-dd' });


						fromDateInput = formGroupDiv.clone().append($(
								'<input/>',
								{
									id : 'fromDatepickerMu',
									name : 'fromDate',
									class : 'form-control',
									type : 'text',
									placeholder : 'From'
								}));

						toDateInput = formGroupDiv.clone().append($(
								'<input/>',
								{
									id : 'toDatepickerMu',
									name : 'toDate',
									class : 'form-control',
									type : 'text',
									placeholder : 'To'
								}));
						
						buttonGetMatchesUnplayed = $(
								'<button/>',
								{
									id : 'buttonGetMatchesUnplayed',
									text : 'Get matches',
									class : 'btn btn-default margin-right-10', 
									click : function() {
										tableMatchesUnplayed.ajax.url('<c:url value="/admin/getMatchesUnplayed/' + 
												'?fromDate=' + $('#fromDatepickerMu').val() + '&toDate=' + $('#toDatepickerMu').val() + '"/>').load();
									}
								});

						$("#dataTables-matches-unplayed_wrapper div.toolbar").append(fromDateInput, toDateInput, buttonGetMatchesUnplayed);

						$( "#fromDatepickerMu" ).datepicker({ dateFormat: 'yy-mm-dd' });
						$( "#toDatepickerMu" ).datepicker({ dateFormat: 'yy-mm-dd' });

						ajaxFormCall('saveFileMpToDbForm', '', 'saveFileMpToDbBtn', function(){$('#listMpFilesBtn').click();});
						
						ajaxButtonCall('listMpFilesBtn', "<c:url value='/admin/listMpFiles'/>",
								function(response){
									var data = $.parseJSON(response);
									$.each(data, function(i, item) {
										$('#saveFileMpToDbForm select').append("<option>" + item.name + "</option>");
									});
								},
								null,
								function(){
									$('#saveFileMpToDbForm select').find('option').remove().end();
								}
						);

						$('#listMpFilesBtn').click();

						$('#callCrawlerDate').datepicker({
							minDate: -7,
							maxDate: -1,
							dateFormat: 'yy-mm-dd'
						});
						
						ajaxFormCall('callCrawlerForm', '', 'callCrawlerBtn', null, null, function(){enableButton('callCrawlerBtn');});






						ajaxFormCall('saveFileMuToDbForm', '', 'saveFileMuToDbBtn', function(){$('#listMuFilesBtn').click()});
							
						ajaxButtonCall('listMuFilesBtn', "<c:url value='/admin/listMuFiles'/>",
								function(response){
									var data = $.parseJSON(response);
									$.each(data, function(i, item) {
										$('#saveFileMuToDbForm select').append("<option>" + item.name + "</option>");
									});
								},
								null,
								function(){
									$('#saveFileMuToDbForm select').find('option').remove().end();
								}
						);

						$('#listMuFilesBtn').click();

						$('#callCrawlerDate2').datepicker({
							minDate: 0,
							maxDate: 7,
							dateFormat: 'yy-mm-dd'
						});
						
						ajaxFormCall('callCrawlerForm2', '', 'callCrawlerBtn2', null, null, function(){enableButton('callCrawlerBtn2');});
					});
	</script>
</body>

</html>