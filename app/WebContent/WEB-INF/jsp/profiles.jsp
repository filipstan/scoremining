<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Admin Profiles</title>

<!-- Bootstrap Core CSS -->
<link
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<c:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="/resources/dist/css/sb-admin-2.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/dist/css/admin.css"/>"
	rel="stylesheet">


<!-- Custom Fonts -->
<link
	href="<c:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="wrapper">

		<jsp:include page="sidebar.jsp"></jsp:include>


		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Profiles</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-info">
						<div class="panel-heading">Profiles List</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<button id="profileListRefreshBtn" type="button"
								class="btn btn-info">
								Refresh <i class="hidden fa fa-gear fa-spin"></i>
							</button>
							<div class="table-responsive">
								<table class="table table-hover" id="profilesList">
									<thead>
										<tr>
											<th>Id</th>
											<th>Name</th>
											<th class="text-center">Actions</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-6 -->
				<div class="col-lg-6">
					<div class="panel panel-yellow">
						<div class="panel-heading">New Profile</div>
						<div class="panel-body">
							<div id="newProfileMsg" class="hidden"></div>
							<form role="form" method="POST" id="newProfileForm"
								action='<c:url value="/admin/profiles/newProfile/"/>'>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Name</label> <input name="name" class="form-control" />
									</div>
									<div class="form-group">
										<label>MinSup (%)</label> <input name="minSup"
											class="form-control" />
										<p class="help-block">Percent between (0, 100]. Integer value.</p>
									</div>
									<div class="form-group">
										<label>MaxGap (seconds)</label> <input name="maxGap"
											class="form-control" />
										<p class="help-block">A day has 86400 seconds.</p>
									</div>
									<div class="form-group">
										<label>Sequence Max Length</label> <input name="sequenceMaxLength"
											class="form-control" />
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group" style="min-height: 200px; max-height: 340px; overflow-y: scroll;">
										<label>Competition</label>
										<div id="competitions" class="checkbox">
											No competitions
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label>Score Types</label> <select multiple name="scoreTypes"
											class="form-control" style="min-height: 150px;">
											<c:forEach items="${scoreTypes}" var="scoreType">
												<option value="${scoreType}">${scoreType}</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-group">
										<label>Strategy Type</label> <select name="strategyType"
											class="form-control">
											<c:forEach items="${strategyTypes}" var="strategyType">
												<option value="${strategyType}">${strategyType}</option>
											</c:forEach>
										</select>
									</div>
									<div class="form-group">
										<label>Trust Formula</label> <input name="trustFormula"
											class="form-control" />
										<p class="help-block">Operators allowed: " ( + - * / ^ sqrt
											) ". Operands allowed: prefixFrequency, scoreType,
											prefixLength.</p>
									</div>
									<button id="newProfileBtn" type="submit" class="btn btn-warning">Save <i class="hidden fa fa-gear fa-spin"></i></button>
								</div>
							</form>
						</div>
						<div class="panel-footer">New Profile</div>
					</div>
				</div>
				<!-- /.col-lg-6 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="<c:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<c:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>

	<!-- SockJs JavaScript -->
	<script
		src="<c:url value="/resources/vendor/sockjs/sockjs.js"/>"></script>
	<!-- STOMP JavaScript -->
	<script
		src="<c:url value="/resources/vendor/stomp/stomp.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<jsp:include page="admin_js.jsp"></jsp:include>
	<script src="<c:url value="/resources/dist/js/sb-admin-2.js"/>"></script>

	<script>
		$(document).ready(
				function() {

					$('#competitions').empty();

					$.ajax({
					    type: 'GET',
					    url: "<c:url value='/admin/profiles/getAllCompetitions\/'/>"
					}).done(function(response, textStatus, jqXHR) {
						
						if (checkAuth(response, textStatus, jqXHR) == true) {
							
							doneCallback(response);

							var competitions = $('#competitions');
						 	var data = $.parseJSON(response);
						 	var selectAllBtn;
				
							if (data.length == 0) {
								competitions.append("No competitions");
							} else {
								selectAllBtn = "<label><input type='checkbox' onchange='selectAllCheckboxes(this, \"competitions\")'>Select all</label>";
								competitions.append(selectAllBtn);
							}
							
							$.each(data, function(i, item) {
								var checkbox = "";
								checkbox += "<label>"
								checkbox += "<input type='checkbox' name='competitions' value='" + item.id + "'>";
								checkbox += item.name + " - " + item.country;
								checkbox += "</label>";
								competitions.append(checkbox);
							});
						}
					    
					}).fail(function(response) {
						
						failCallback(response);
					});

					ajaxFormCall('newProfileForm', 'newProfileMsg',
							'newProfileBtn', function(){refreshBtn.click();});

					var table = $('#profilesList');
					var refreshBtn = $('#profileListRefreshBtn');
					var refreshBtnIcon = $('#profileListRefreshBtn i');

					refreshBtn.click(function() {

						refreshBtn.addClass('disabled');
						refreshBtnIcon.removeClass('hidden');

						table.find('tbody tr').remove().end();

						$.ajax({
							type : 'GET',
							url : "<c:url value='/admin/profiles/getAllProfiles/'/>"
						}).done(
							function(response) {
	
								var data = $.parseJSON(response);
								$.each(data, function(i, item) {
									
									var tr = '';
									
									tr += '<tr>';
									tr += '<td>' + item.id + '</td>';
									tr += '<td>' + item.name + '</td>';
									tr += '<td id="popover-container' + item.id + '" class="text-center"><button type="button" class="btn btn-info btn-circle margin-right-10 popover-link popover-toggle" title="Details ' + item.name + '"><i class="fa fa-list-alt"></i></button>';
									tr += '<button type="button" data-profile-id="' + item.id + '" class="btn btn-danger btn-circle margin-right-10 remove-profile" title="Remove"><i class="fa fa-remove"></i></button></td>';
									tr += '<td class="hidden popover-content">';
									tr += '<p><b>MinSup:</b> ' + item.minSup + '%</p>';
									tr += '<p><b>MaxGap:</b> ' + item.maxGap + ' seconds</p>';
									tr += '<p><b>Sequence Max Length:</b> ' + item.sequenceMaxLength + '</p>';
									tr += '<p><b>Score Types:</b><br/>' + item.scoreTypes + '</p>';
									tr += '<p><b>Strategy Type:</b> ' + item.strategyType + '</p>';
									tr += '<p><b>Trust Formula:</b> ' + item.trustFormula + '</p>';
									if (item.competitions.length > 0) {
										tr += '<p><b>Competitions:</b>';
										$.each(item.competitions, function(j, profileCompetition) {
											tr += '<br/>' + profileCompetition + '</p>';
										});
									}
									tr += '</td></tr>';
									
									table.find('tbody').append(tr);
								});

								$('.popover-link').each(function(i){
									var id = $(this).parent().attr('id')
									$(this).popover({
										html : true,
										container : '#' + id,
										content : function() {
											return $(this).closest('tr').find('.popover-content').html();
										}
									});
								});

								$('.remove-profile').click(function(){
									var id = parseInt($(this).data('profile-id'));
									$.ajax({
										type : 'GET',
										url : "<c:url value='/admin/profiles/removeProfile/?id=" + id + "'/>"
									}).done(function(response){
										refreshBtn.click();
									}).fail(function(response){
										refreshBtn.click();
									});
								});
	
								refreshBtnIcon.addClass('hidden');
								refreshBtn.removeClass('disabled');
	
							}).fail(function(response) {
	
								refreshBtnIcon.addClass('hidden');
								refreshBtn.removeClass('disabled');
							});
					});

					refreshBtn.click();
				});
	</script>

</body>

</html>