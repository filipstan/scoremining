<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script>
	function isLoggedIn() {
		return '<c:if test="${pageContext.request.userPrincipal.name != null}">true</c:if>';
	}
</script>
