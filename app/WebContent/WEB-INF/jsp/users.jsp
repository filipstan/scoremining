<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Admin Users</title>

<!-- Bootstrap Core CSS -->
<link
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<c:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="/resources/dist/css/sb-admin-2.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/dist/css/admin.css"/>"
	rel="stylesheet">


<!-- Custom Fonts -->
<link
	href="<c:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="wrapper">

		<jsp:include page="sidebar.jsp"></jsp:include>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Users</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">Users List</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Id</th>
											<th>Email</th>
											<th>Roles</th>
											<th>Enabled</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${users}" var="user">
											<tr>
												<td>${user.id}</td>
												<td>${user.email}</td>
												<td><c:forEach items="${user.roles}" var="userRole">
														${userRole.name} 
													</c:forEach></td>
												<td>
													<c:if test="${user.enabled == true}">YES</c:if>
													<c:if test="${user.enabled == false}">NO</c:if>
												</td>
												<td>
													<button type="button"
														class="btn btn-primary btn-circle margin-right-10"
														title="Edit">
														<i class="fa fa-edit"></i>
													</button>
													<button type="button"
														class="btn btn-danger btn-circle margin-right-10"
														title="Remove">
														<i class="fa fa-remove"></i>
													</button>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->


	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="<c:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<c:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>

	<!-- SockJs JavaScript -->
	<script
		src="<c:url value="/resources/vendor/sockjs/sockjs.js"/>"></script>
	<!-- STOMP JavaScript -->
	<script
		src="<c:url value="/resources/vendor/stomp/stomp.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<jsp:include page="admin_js.jsp"></jsp:include>
	<script src="<c:url value="/resources/dist/js/sb-admin-2.js"/>"></script>

</body>

</html>