<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Admin - Teams</title>

<!-- Bootstrap Core CSS -->
<link
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<c:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- DataTables CSS -->
<link
	href="<c:url value="/resources/vendor/datatables-plugins/dataTables.bootstrap.css"/>"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link
	href="<c:url value="/resources/vendor/datatables-responsive/dataTables.responsive.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="/resources/dist/css/sb-admin-2.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/dist/css/admin.css"/>"
	rel="stylesheet">


<!-- Custom Fonts -->
<link
	href="<c:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="wrapper">
		<jsp:include page="sidebar.jsp"></jsp:include>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Teams</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseOne">Merge Teams</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<form role="form" name="mergeTeamsForm" id="mergeTeamsForm"
											method="POST" action='<c:url value="/admin/mergeTeams"/>'>
											<div class="form-group">
												<label>Keep Team Id</label> <input name="keepTeamId"
													class="form-control">
											</div>
											<div class="form-group">
												<label>Remove Team Id</label> <input name="removeTeamId"
													class="form-control">
											</div>
											<button id="mergeTeamsFormBtn" type="submit"
												class="btn btn-default">
												Merge <i class="hidden fa fa-gear fa-spin"></i>
											</button>
											<button id="mergeAllTeamsBtn" type="button"
												class="btn btn-default">
												Merge all <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseTwo">Classify Teams</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<button id="classifyTeamsBtn" type="button"
											class="btn btn-default">
											Classify <i class="hidden fa fa-gear fa-spin"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Teams</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-example">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Country</th>
										<th>Level</th>
									</tr>
								</thead>
							</table>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="<c:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<c:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>

	<!-- DataTables JavaScript -->
	<script
		src="<c:url value="/resources/vendor/datatables/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/datatables-responsive/dataTables.responsive.js"/>"></script>
	
	<!-- SockJs JavaScript -->
	<script
		src="<c:url value="/resources/vendor/sockjs/sockjs.js"/>"></script>
	<!-- STOMP JavaScript -->
	<script
		src="<c:url value="/resources/vendor/stomp/stomp.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<jsp:include page="admin_js.jsp"></jsp:include>
	<script src="<c:url value="/resources/dist/js/sb-admin-2.js"/>"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
		var tableTeams;
		
		$(document).ready(
				
						function() {
							
							tableTeams = $('#dataTables-example').DataTable(
											{
												ajax : '<c:url value="/admin/getAllTeamsJSON"/>',
												responsive : true,
												"deferRender": true,
												"dom" : '<"toolbar">frtip'
											});
							
							var buttonAllTeams = $(
									'<button/>',
									{
										id : 'showAllTeamsBtn',
										text : 'Show all',
										class : 'btn btn-default margin-right-10', 
										click : function() {
											tableTeams.ajax
													.url(
															'<c:url value="/admin/getAllTeamsJSON"/>')
													.load();
										}
									});
							var buttonDuplicateTeams = $(
									'<button/>',
									{
										id : 'showDuplicatesTeamsBtn',
										text : 'Show duplicates',
										class : 'btn btn-default margin-right-10',
										click : function() {
											tableTeams.ajax
													.url(
															'<c:url value="/admin/getDuplicateTeamsJSON"/>')
													.load();
										}
									});
							$("div.toolbar").append(buttonAllTeams);
							$("div.toolbar").append(buttonDuplicateTeams);
							
							var form = $('#mergeTeamsForm');
							
							ajaxFormCall('mergeTeamsForm', '', 'mergeTeamsFormBtn');

							ajaxButtonCall('classifyTeamsBtn', "<c:url value='/admin/classifyTeams'/>",
									function(){
										$('#showAllTeamsBtn').click();
									}
							);
							
							ajaxButtonCall('mergeAllTeamsBtn', "<c:url value='/admin/mergeAllTeams'/>");
						});
	</script>
</body>

</html>