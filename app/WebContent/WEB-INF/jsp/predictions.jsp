<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Admin Predictions</title>

<!-- Bootstrap Core CSS -->
<link
	href="<c:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<c:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>"
	rel="stylesheet">

<!-- DataTables CSS -->
<link
	href="<c:url value="/resources/vendor/datatables-plugins/dataTables.bootstrap.css"/>"
	rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link
	href="<c:url value="/resources/vendor/datatables-responsive/dataTables.responsive.css"/>"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="/resources/dist/css/sb-admin-2.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/dist/css/admin.css"/>"
	rel="stylesheet">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<!-- Custom Fonts -->
<link
	href="<c:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="wrapper">

		<jsp:include page="sidebar.jsp"></jsp:include>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Predictions</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-primary">
                                        
						<div class="panel-heading panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="no-hover-effects">
								<h4 class="panel-title">
								Predictions for matches unplayed
								</h4>
							</a>
						</div>
						<div class="panel-body collapse" id="collapseOne">
							<div class="row">
								<div class="col-lg-12">
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" placeholder="From" id="fromPredictionMuDate">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" placeholder="To" id="toPredictionMuDate">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<button type="button" id="predictionGetMuBtn" class="btn btn-default">
												Get matches <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</div>
									</div>

									<div class="col-lg-12">
										<form role="form" id="predictionMuForm" action='<c:url value="/admin/predictions/getPredictionsMu/"/>'>
											<div class="form-group" style="min-height: 200px; max-height: 200px; overflow-y: scroll;">
												<label>Matches unplayed</label>
												<div class="checkbox">
													No matches
												</div>
											</div>
										</form>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Use profile</label> <select id="predictionMuProfileSelect" name="profile" form="predictionMuForm" class="form-control"></select>
										</div>
									</div>
									<div id="popover-containerOne" class="col-lg-6">
										<div class="form-group">
											<button type="button"
												class="btn btn-outline btn-default  btn-block popover-link popover-toggle"
												style="margin-top: 25px;">
												<i class="fa fa-list-alt"></i> Details
											</button>
										</div>
										<div class="hidden popover-content-custom"></div>
									</div>

									<button type="submit" form="predictionMuForm"
										id="predictionMuBtn"
										class="btn btn-outline btn-primary btn-lg btn-block">
										Compute <i class="hidden fa fa-gear fa-spin"></i>
									</button>
								</div>
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				
				<div class="col-lg-6">
					<div class="panel panel-primary">
						<div class="panel-heading panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" class="no-hover-effects">
								<h4 class="panel-title">
								Predictions for matches played
								</h4>
							</a>
						</div>
						<div class="panel-body collapse" id="collapseTwo">
							<div class="row">
								<div class="col-lg-12">
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" placeholder="From" id="fromPredictionMpDate">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<input class="form-control" placeholder="To" id="toPredictionMpDate">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<button type="button" id="predictionGetMpBtn" class="btn btn-default">
												Get matches <i class="hidden fa fa-gear fa-spin"></i>
											</button>
										</div>
									</div>

									<div class="col-lg-12">
										<form role="form" id="predictionMpForm" action='<c:url value="/admin/predictions/getPredictionsMp/"/>'>
											<div class="form-group" style="min-height: 200px; max-height: 200px; overflow-y: scroll;">
												<label>Matches played</label>
												<div class="checkbox">
													No matches
												</div>
											</div>
										</form>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Use profile</label> <select id="predictionMpProfileSelect" name="profile" form="predictionMpForm" class="form-control"></select>
										</div>
									</div>
									<div id="popover-containerTwo" class="col-lg-6">
										<div class="form-group">
											<button type="button"
												class="btn btn-outline btn-default  btn-block popover-link popover-toggle"
												style="margin-top: 25px;">
												<i class="fa fa-list-alt"></i> Details
											</button>
										</div>
										<div class="hidden popover-content-custom"></div>
									</div>

									<button type="submit" form="predictionMpForm"
										id="predictionMpBtn"
										class="btn btn-outline btn-primary btn-lg btn-block">
										Compute <i class="hidden fa fa-gear fa-spin"></i>
									</button>
								</div>
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
			</div>
			<!-- /.row -->
			
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Matches Predictions</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-matches-predictions">
								<thead>
									<tr>
										<th>Match</th>
										<th>Score Predicted</th>
										<th>Score Type</th>
										<th>Trust</th>
										<th>Profile</th>
									</tr>
								</thead>
							</table>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Matches Predictions Grouped</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<table class="table table-striped table-bordered table-hover"
								id="dataTables-matches-predictions-grouped">
								<thead>
									<tr>
										<th>Match</th>
										<th>Score</th>
										<th>ScoreP</th>
										<th>ScoreP2</th>
										<th>Conf</th>
										<th>bFactor</th>
										<th>Competition</th>
										<th>Country</th>
										<th>Date</th>
										<th>Profile</th>
									</tr>
								</thead>
							</table>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="<c:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<c:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<c:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>
	
	<!-- DataTables JavaScript -->
	<script
		src="<c:url value="/resources/vendor/datatables/js/jquery.dataTables.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"/>"></script>
	<script
		src="<c:url value="/resources/vendor/datatables-responsive/dataTables.responsive.js"/>"></script>
	
	<!-- SockJs JavaScript -->
	<script
		src="<c:url value="/resources/vendor/sockjs/sockjs.js"/>"></script>
	<!-- STOMP JavaScript -->
	<script
		src="<c:url value="/resources/vendor/stomp/stomp.js"/>"></script>

	<!-- Custom Theme JavaScript -->
	<jsp:include page="admin_js.jsp"></jsp:include>
	<script src="<c:url value="/resources/dist/js/sb-admin-2.js"/>"></script>
	
	<script>
	$(document).ready(function(){
		$("#fromPredictionMuDate").datepicker({dateFormat: 'yy-mm-dd'});
		$("#toPredictionMuDate").datepicker({dateFormat: 'yy-mm-dd'});
		
		ajaxButtonCall('predictionGetMuBtn',
			"<c:url value='/admin/getMatchesUnplayed/'/>",
			function(response){
			 	var predictionMuForm = $('#predictionMuForm');
			 	var data = $.parseJSON(response);
			 	var selectAllBtn;
				data = data.data;
	
				if (data.length == 0) {
					predictionMuForm.find('.checkbox').append("No matches");
				} else {
					selectAllBtn = "<label><input type='checkbox' onchange='selectAllCheckboxes(this, \"matches\")'>Select all</label>";
					predictionMuForm.find('.checkbox').append(selectAllBtn);
				}
				
				$.each(data, function(i, item) {
					var checkbox = "";
					checkbox += "<label>"
					checkbox += "<input type='checkbox' name='matches' value='" + item[0] + "'>";
					checkbox += item[1] + " - " + item[2] + " (" + item[3] + " - " + item[4] + ") " + item[5];
					checkbox += "</label>";
					predictionMuForm.find('.checkbox').append(checkbox);
				});
			},
			null,
			function(){
			 	$('#predictionMuForm').find('.checkbox').empty();
			},
			function(){
				return "?fromDate=" + $("#fromPredictionMuDate").val()
						 + "&toDate=" + $("#toPredictionMuDate").val();
			}
		);

		populateProfiles();

		$('#predictionMuProfileSelect, #predictionMpProfileSelect').change(function(){

			var profileId = $(this).val();
			var container = $(this).attr('id') == 'predictionMuProfileSelect' ? 
					$(this).closest('.panel-body').find('#popover-containerOne') : 
						$(this).closest('.panel-body').find('#popover-containerTwo');
			var popoverLink = container.find('.popover-link');
			var popoverContent = container.find('.popover-content-custom')

			popoverLink.addClass('disabled').prop('disabled', true);

			$.ajax({
				type : 'GET',
				url : "<c:url value='/admin/profiles/getAllProfiles/?id=" + profileId + "'/>"
			}).done(function(response) {
				
				var data = $.parseJSON(response);
				var item = data[0];
				popoverContent.html(getProfileHtml(item));
				popoverLink.attr('data-original-title', 'Details ' + item.name);

				popoverLink.popover({
					html : true,
					placement : 'top',
					container : '#' + container.attr('id'),
					content : function() {
						return popoverContent.html();
					}
				});
				
				popoverLink.removeClass('disabled').removeAttr('disabled');
			}).fail(function(response) {
				//popoverLink.popover('dispose');
				popoverLink.removeClass('disabled').removeAttr('disabled');
			});
		});

		ajaxFormCall('predictionMuForm', '', 'predictionMuBtn');








		$("#fromPredictionMpDate").datepicker({dateFormat: 'yy-mm-dd'});
		$("#toPredictionMpDate").datepicker({dateFormat: 'yy-mm-dd'});
		
		ajaxButtonCall('predictionGetMpBtn',
			"<c:url value='/admin/getMatchesPlayed/'/>",
			function(response){
		 		var predictionMpForm = $('#predictionMpForm');
		 		var data = $.parseJSON(response);
		 		var selectAllBtn;
				data = data.data;
	
				if (data.length == 0) {
					predictionMpForm.find('.checkbox').append("No matches");
				} else {
					selectAllBtn = "<label><input type='checkbox' onchange='selectAllCheckboxes(this)'>Select all</label>";
					predictionMpForm.find('.checkbox').append(selectAllBtn);
				}
				
				$.each(data, function(i, item) {
					var checkbox = "";
					checkbox += "<label>"
					checkbox += "<input type='checkbox' name='matches' value='" + item[0] + "'>";
					checkbox += item[1] + " - " + item[2] + " " + item[3] + " (" + item[4] + " - " + item[5] + ") " + item[6];
					checkbox += "</label>";
					predictionMpForm.find('.checkbox').append(checkbox);
				});
			},
			null,
			function(){
				$('#predictionMpForm').find('.checkbox').empty();
			},
			function (){
				return "?fromDate=" + $("#fromPredictionMpDate").val()
						 + "&toDate=" + $("#toPredictionMpDate").val();
			}
		);
		
		ajaxFormCall('predictionMpForm', '', 'predictionMpBtn');




		tableMatchesPredictions = $('#dataTables-matches-predictions')
		.DataTable(
				{
					/*ajax: {
			            "url": '<c:url value="/admin/getMatchesPredictions/"/>',
			            "type": "POST"
			        },*/
					responsive : true,
					"deferRender": true,
					"dom" : '<"toolbar">frtip',
					columnDefs: [
			            { width: "45%", targets: 0 }
			        ]
				});
		tableMatchesPredictionsGrouped = $('#dataTables-matches-predictions-grouped')
		.DataTable(
				{
					ajax: {
			            "url": '<c:url value="/admin/getMatchesPredictionsGrouped/"/>',
			            "type": "POST"
			        },
					responsive : true,
					"deferRender": true,
					"dom" : '<"toolbar">frtip'
				});

		$('#dataTables-matches-predictions-grouped')
		.on('draw.dt', function ( e, settings, json, xhr ) {
	        $.each(settings.nTBody.childNodes, function(i){
		        var data = tableMatchesPredictionsGrouped.row(this).data();
		        if (typeof data !== "undefined"){
					$(this).find("td:eq(2)").attr('title', data[10]);

					s1 = $(this).find("td:eq(1)").text();
					s2 = $(this).find("td:eq(2)").text();
					s3 = $(this).find("td:eq(3)").text();

					if (s1.indexOf("-") == -1 
							|| s2.indexOf("-") == -1
							|| s3.indexOf("-") == -1) {
						return;
					}

					s1 = parseInt(s1.substring(0, s1.indexOf("-"))) - parseInt(s1.substring(s1.indexOf("-") + 1));
					s2 = parseInt(s2.substring(0, s2.indexOf("-"))) - parseInt(s2.substring(s2.indexOf("-") + 1));
					s3 = parseInt(s3.substring(0, s3.indexOf("-"))) - parseInt(s3.substring(s3.indexOf("-") + 1));

					if ((s1 > 0 && s2 > 0)
							|| (s1 == 0 && s2 == 0)
							|| (s1 < 0 && s2 < 0)) {
						$(this).find("td:eq(1)").removeClass().addClass("background_light_green");
						$(this).find("td:eq(2)").removeClass().addClass("background_light_green");
					}

					if ((s1 > 0 && s3 > 0)
							|| (s1 == 0 && s3 == 0)
							|| (s1 < 0 && s3 < 0)) {
						$(this).find("td:eq(1)").removeClass().addClass("background_light_green");
						$(this).find("td:eq(3)").removeClass().addClass("background_light_green");
					}
		        }
	        });
	    });

		var formGroupDiv = $(
			'<div/>',
			{
				class : 'form-group margin-right-10'
			});
       
		var fromDateInput = formGroupDiv.clone().append($(
				'<input/>',
				{
					id : 'fromDatepickerMpr',
					name : 'fromDate',
					class : 'form-control',
					type : 'text',
					placeholder : 'From'
				}));

		var toDateInput = formGroupDiv.clone().append($(
				'<input/>',
				{
					id : 'toDatepickerMpr',
					name : 'toDate',
					class : 'form-control',
					type : 'text',
					placeholder : 'To'
				}));
		
		var buttonGetMatchesPr = $(
				'<button/>',
				{
					id : 'buttonGetMatchesPr',
					text : 'Get matches predictions',
					class : 'btn btn-default margin-right-10', 
					click : function() {
						tableMatchesPredictions.ajax.url('<c:url value="/admin/getMatchesPredictions/' + 
								'?fromDate=' + $('#fromDatepickerMpr').val() + '&toDate=' + $('#toDatepickerMpr').val() + '"/>').load();
					}
				});

		var buttonDeleteMatchesPr = $(
				'<button/>',
				{
					id : 'buttonDeleteMatchesPr',
					text : 'Delete matches predictions',
					class : 'btn btn-default margin-right-10'
				});

		

		$("#dataTables-matches-predictions_wrapper div.toolbar").append(fromDateInput, toDateInput, buttonGetMatchesPr, buttonDeleteMatchesPr);

		ajaxButtonCall('buttonDeleteMatchesPr',
				"<c:url value='/admin/deleteMatchesPredictions/'/>",
				function(response){
					if (response != "") {
						tableMatchesPredictions.ajax.url('<c:url value="/admin/getMatchesPredictions/' + 
							'?fromDate=' + $('#fromDatepickerMpr').val() + '&toDate=' + $('#toDatepickerMpr').val() + '"/>').load();
					}
				},
				null,
				null,
				function (){
					return "?fromDate=" + $("#fromDatepickerMpr").val()
							 + "&toDate=" + $("#toDatepickerMpr").val();
				}, true,
				function(){return "Delete all predictions between " + $("#fromDatepickerMpr").val() + " and " + $("#toDatepickerMpr").val() + "?"}
			);
		
		$( "#fromDatepickerMpr" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#toDatepickerMpr" ).datepicker({ dateFormat: 'yy-mm-dd' });



		fromDateInput = formGroupDiv.clone().append($(
				'<input/>',
				{
					id : 'fromDatepickerMprg',
					name : 'fromDate',
					class : 'form-control',
					type : 'text',
					placeholder : 'From'
				}));

		toDateInput = formGroupDiv.clone().append($(
				'<input/>',
				{
					id : 'toDatepickerMprg',
					name : 'toDate',
					class : 'form-control',
					type : 'text',
					placeholder : 'To'
				}));

		var onlyTrustyCheckbox = formGroupDiv.clone().append(
				$('<label style="font-weight:400;margin-bottom:0;">'
						 + '<input id="onlyTrusty" type="checkbox" '
						 + 'name="onlyTrusty" class="form-control"/>'
						 + 'Only trusty</label>')
			);
		
		buttonGetMatchesPrg = $(
				'<button/>',
				{
					id : 'buttonGetMatchesPrg',
					text : 'Get matches predictions grouped',
					class : 'btn btn-default margin-right-10', 
					click : function() {
						tableMatchesPredictionsGrouped.ajax.url('<c:url value="/admin/getMatchesPredictionsGrouped/' + 
								'?fromDate=' + $('#fromDatepickerMprg').val()
								 + '&toDate=' + $('#toDatepickerMprg').val()
								 + '&onlyTrusty=' + $('#onlyTrusty').is(':checked') + '"/>').load();
					}
				});

		

		$("#dataTables-matches-predictions-grouped_wrapper div.toolbar").append(
				fromDateInput, toDateInput, onlyTrustyCheckbox, buttonGetMatchesPrg);

		$( "#fromDatepickerMprg" ).datepicker({ dateFormat: 'yy-mm-dd' });
		$( "#toDatepickerMprg" ).datepicker({ dateFormat: 'yy-mm-dd' });
	});

	function populateProfiles(){
		
		$.ajax({
			type : 'GET',
			url : "<c:url value='/admin/profiles/getAllProfiles/'/>"
		}).done(function(response) {

			var data = $.parseJSON(response);
			
			$('#predictionMuProfileSelect').empty();
			$('#predictionMpProfileSelect').empty();
			
			$.each(data, function(i, item) {
				var option = '<option value="' + item.id + '">' + item.name + '</option>';
				$('#predictionMuProfileSelect').append(option);
				$('#predictionMpProfileSelect').append(option);
			});
			
			$('#predictionMuProfileSelect').change();
			$('#predictionMpProfileSelect').change();
			
		}).fail(function(response) {
			$('#predictionMuProfileSelect').empty();
			$('#predictionMpProfileSelect').empty();
		});
	}

	function getProfileHtml(item) {
		
		var content = '';
		
		content += '<p><b>MinSup:</b> ' + item.minSup + '%</p>';
		content += '<p><b>MaxGap:</b> ' + item.maxGap + ' seconds</p>';
		content += '<p><b>Sequence Max Length:</b> ' + item.sequenceMaxLength + '</p>';
		content += '<p><b>Score Types:</b><br/>' + item.scoreTypes + '</p>';
		content += '<p><b>Strategy Type:</b> ' + item.strategyType + '</p>';
		content += '<p><b>Trust Formula:</b> ' + item.trustFormula + '</p>';
		if (item.competitions.length > 0) {
			content += '<p><b>Competitions:</b>';
			$.each(item.competitions, function(j, profileCompetition) {
				content += '<br/><span class="profileCompetition">' + profileCompetition + '</span></p>';
			});
		}
		content += '</td></tr>';
		
		return content;
	}
	
	</script>

</body>

</html>