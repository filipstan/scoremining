<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/j_spring_security_logout" var="logoutUrl" />
<form action="${logoutUrl}" method="post" id="logoutForm">
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</form>
<script>
	function formSubmit() {
		document.getElementById("logoutForm").submit();
	}
</script>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation"
	style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<c:url value="/admin/"/>">Administration Panel</a>
	</div>
	<!-- /.navbar-header -->

	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	            <i class="fa fa-bell fa-fw" id="notificationsBtn"></i> <i class="fa fa-caret-down"></i>
	        </a>
	        <ul class="dropdown-menu dropdown-messages" id="notifications">
	            <li>
	                <a class="text-center" href="#" id="moreNotificationsBtn">
	                    <strong>More</strong>
	                    <i class="fa fa-angle-right"></i>
	                </a>
	            </li>
	        </ul>
	        <!-- /.dropdown-messages -->
	    </li>
		<li class="dropdown"><a class="dropdown-toggle"
			data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
				<i class="fa fa-caret-down"></i>
		</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i>
						${pageContext.request.userPrincipal.name}</a></li>
				<li class="divider"></li>
				<li><a href="javascript:formSubmit()"><i
						class="fa fa-sign-out fa-fw"></i> Logout</a></li>

			</ul> <!-- /.dropdown-user --></li>
		<!-- /.dropdown -->
	</ul>
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li><a href="<c:url value="/admin/"/>"><i
						class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
				<li><a href="<c:url value="/admin/teams/"/>"><i
						class="fa fa-list-ul fa-fw"></i> Teams</a></li>
				<li><a href="<c:url value="/admin/matches/"/>"><i
						class="fa fa-th-large fa-fw"></i> Matches</a></li>
				<li><a href="<c:url value="/admin/predictions/"/>"><i
						class="fa fa-random fa-fw"></i> Predictions</a></li>
				<li><a href="<c:url value="/admin/users/"/>"><i
						class="fa fa-users fa-fw"></i> Users</a></li>
				<li><a href="<c:url value="/admin/profiles/"/>"><i
						class="fa fa-gears fa-fw"></i> Profiles</a></li>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
</nav>