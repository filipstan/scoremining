/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */





/*
 * Global variables
 */
var stompClient = null;




/*
 * Run on first load
 */
$(function() {
	
	$('#side-menu').metisMenu();
	
	connectWebSocket();
	
	$('#moreNotificationsBtn').click(function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		showLastNotifications();
	});
});

// Loads the correct sidebar on window load,
// collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
	$(window)
			.bind(
					"load resize",
					function() {
						var topOffset = 50;
						var width = (this.window.innerWidth > 0) ? this.window.innerWidth
								: this.screen.width;
						if (width < 768) {
							$('div.navbar-collapse').addClass('collapse');
							topOffset = 100; // 2-row-menu
						} else {
							$('div.navbar-collapse').removeClass('collapse');
						}

						var height = ((this.window.innerHeight > 0) ? this.window.innerHeight
								: this.screen.height) - 1;
						height = height - topOffset;
						if (height < 1)
							height = 1;
						if (height > topOffset) {
							$("#page-wrapper").css("min-height",
									(height) + "px");
						}
					});

	var url = window.location;
	// var element = $('ul.nav a').filter(function() {
	// return this.href == url;
	// }).addClass('active').parent().parent().addClass('in').parent();
	var element = $('ul.nav a').filter(function() {
		return this.href == url;
	}).addClass('active').parent();

	while (true) {
		if (element.is('li')) {
			element = element.parent().addClass('in').parent();
		} else {
			break;
		}
	}
});






/*
 * Set atributtes functions
 */
function disableButton(btnId) {
	var btn = $('#' + btnId);
	var btnIcon = $('#' + btnId + ' i');
	
	btn.addClass('disabled').prop('disabled', true);
	btnIcon.removeClass('hidden');
}

function enableButton(btnId) {
	var btn = $('#' + btnId);
	var btnIcon = $('#' + btnId + ' i');
	
	btnIcon.addClass('hidden');
	btn.removeClass('disabled').removeAttr('disabled');
}

function selectAllCheckboxes(_this, name) {
	var _this = $(_this);
	var profileCompetitions = _this.closest('.panel-body').find('.profileCompetition');
	
	if (profileCompetitions.length > 0) {
		_this.closest('.checkbox').find('[name=' + name + ']').each(function(index) {
			var checkbox = $(this);
			var label = checkbox.parent().text();
			$.each(profileCompetitions, function (i, profileCompetition) {
				if (label.indexOf('(' + $(profileCompetition).text() + ')') > -1) {
					checkbox.prop('checked', _this[0].checked);
					return;
				}
			});
		});
	} else {
		_this.closest('.checkbox').find('[name=' + name + ']').prop('checked', _this[0].checked);
	}
}






/*
 * Ajax calls functions 
 */

//param formMsgId is not used (obsolete)
//var formMsg = $('#' + formMsgId);
function ajaxFormCall(formId, formMsgId, submitBtnId, successCallback, failureCallback, afterInitCallback) {
	
	var form = $('#' + formId);

	form.submit(function(ev) {

		ev.preventDefault();

		disableButton(submitBtnId);
		
		if (isFunction(afterInitCallback)) {
			afterInitCallback();
		}

		var formData = form.serialize();

		$.ajax({
			type : 'POST',
			url : form.attr('action'),
			data : formData
		}).done(function(response, textStatus, jqXHR) {
			
			if (checkAuth(response, textStatus, jqXHR) == true) {

				doneCallback(response);
				
				form[0].reset();
				
				if (isFunction(successCallback)) {
					successCallback();
				}
			}

			enableButton(submitBtnId);

		}).fail(function(response) {
			
			failCallback(response);

			form[0].reset();
			
			if (isFunction(failureCallback)) {
				failureCallback();
			}

			enableButton(submitBtnId);
		});
	});
}

function ajaxButtonCall(buttonId, url, succesCallback, failureCallback, afterInitCallback, queryUrlCallback, confirmFlag, confirmMsg) {
	
	var button = $('#' + buttonId);
	var urlWithParams = url;
	
	button.click(function(){
		
		if (confirmFlag) {
			if (isFunction(confirmMsg)) {
				if (!confirm(confirmMsg())) {
					return;
				}
			} else if (!confirm("Please confirm.")) {
				return;
			}
		}
		
		if (isFunction(queryUrlCallback)) {
			urlWithParams = url + queryUrlCallback();
		}
		
		disableButton(buttonId);
		
		if (isFunction(afterInitCallback)) {
			afterInitCallback();
		}
		
		$.ajax({
		    type: 'GET',
		    url: urlWithParams
		}).done(function(response, textStatus, jqXHR) {
			
			if (checkAuth(response, textStatus, jqXHR) == true) {
				
				doneCallback(response);
				
				if (isFunction(succesCallback)) {
					succesCallback(response);
				}
			}
		    
		    enableButton(buttonId);
		    
		}).fail(function(response) {
			
			failCallback(response);
			
			if (isFunction(failureCallback)) {
				failureCallback();
			}

		    enableButton(buttonId);
		});
	});
}




/*
 * Check functions
 */

function isFunction(functionCallback) {
	return (typeof functionCallback === "function");
}

function checkAuth(response, textStatus, jqXHR) {
	
	var loggedUser = /function\s*isLoggedIn\(\)\s*\{\s*return\s*['|"](.*)['|"];\s*\}/g.exec(response);;
	
	if (loggedUser !== null) {
		if (loggedUser[1] != '') {
			return true;
		} else {
			var r = confirm("Session expired. Go to login page?");
			if (r == true) {
				location.reload();
			}
			return false;
		}
	}
	
	return true;
}





/*
 * Callback functions
 */

function doneCallback(response) {
	console.log(response);
}

function failCallback(response) {
	console.log(response);
}





/*
 * WebSocket functions 
 */

function connectWebSocket() {
	
	if (!isLoggedIn()){
		console.log('Cannot connect websocket until you log in.');
		return;
	}
	
    var socket = new SockJS('/scoremining/notifications');
    stompClient = Stomp.over(socket);  
    stompClient.connect({}, function(frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/messages', function(messageOutput) {
        	showNotification(JSON.parse(messageOutput.body));
        });
        stompClient.subscribe('/topic/messages/progress', function(messageOutput) {
        	updateNotificationProgress(JSON.parse(messageOutput.body));
        });
        $('#moreNotificationsBtn').click();
    });
}

function disconnectWebSocket() {
    if(stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function sendMessage() {
    stompClient.send("/scoremining/admin/notification/getLast/ws", {}, 
      JSON.stringify({limit: "5", beforeId: 0}));
}

function updateNotificationProgress(response) {
	var notificationId = response.notificationId;
	var progress = response.progress;
	
	$('#n-' + notificationId + ' .n-message-progress').text(progress);
}

function showNotification(response) {
	var i, li;
    var notifications = $('#notifications');
    
    if (!response.data) {
    	li = createNotificationElement(response);
	    li.prependTo(notifications);
	    
	    $('#notificationsBtn').addClass("shake red");
	    setTimeout(function(){$('#notificationsBtn').removeClass("shake red");}, 2000);
    } else {
    	for (i = 0; i < response.data.length; i++) {
    		li = createNotificationElement(response.data[i]);
    		li.insertBefore(notifications.find('li').last());
    	}
    }
}

function createNotificationElement(notification) {
	return $(
		'<li id="n-' + notification.id + '" class="notification">' + 
            '<a href="javascript:void(0)">' + 
            '<div>' + 
            '<strong>' + notification.user + '</strong>' + 
            '<span class="pull-right text-muted"><em>' + notification.timestamp + '</em></span>' + 
            '</div>' + 
            '<div class="n-message">' + notification.message + '</div>' + 
            '<div class="text-right text-muted small n-message-progress"></div>' + 
         '</a>' + 
       '</li>' + 
    '<li class="divider"></li>');
}

function showLastNotifications() {
	var notifications = $('#notifications');
	var lastNotification = notifications.find('.notification').last();
	var idLastNotification = 0;
	
	if (lastNotification.length > 0) {
		idLastNotification = lastNotification.attr('id');
		idLastNotification = idLastNotification.substr(idLastNotification.indexOf('n-') + 2);
	}
	
	stompClient.send("/scoremining/admin/notification/getLast/ws", {}, 
		      JSON.stringify({limit: "5", beforeId: idLastNotification + ""}));
}

