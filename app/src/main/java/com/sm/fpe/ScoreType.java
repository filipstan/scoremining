package com.sm.fpe;

public enum ScoreType {
	
	HomeScoreHomeTeamGiven(20),
	AwayScoreAwayTeamGiven(20),
	HomeScoreAwayTeamTaken(15),
	AwayScoreHomeTeamTaken(15),
	HomeAndAwayScoreHomeTeamGiven(10),
	HomeAndAwayScoreAwayTeamGiven(10),
	HomeAndAwayScoreHomeTeamTaken(5),
	HomeAndAwayScoreAwayTeamTaken(5),
	
	HomeScoreHomeTeamAgainstTeamLevelGiven(40),
	AwayScoreAwayTeamAgainstTeamLevelGiven(40),
	HomeScoreAwayTeamAgainstTeamLevelTaken(35),
	AwayScoreHomeTeamAgainstTeamLevelTaken(35),
	HomeAndAwayScoreHomeTeamAgainstTeamLevelGiven(25),
	HomeAndAwayScoreAwayTeanAgainstTeamLevelGiven(25),
	HomeAndAwayScoreHomeTeamAgainstTeamLevelTaken(20),
	HomeAndAwayScoreAwayTeamAgainstTeamLevelTaken(20);
	
	private final int trust;
	
	ScoreType(int trust){
		this.trust = trust;
	}
	
	public int getTrust() {
		return this.trust;
	}
	
	public boolean isForHomeTeam(){
		switch (this) {
		case HomeScoreHomeTeamGiven:
		case HomeScoreAwayTeamTaken:
		case HomeAndAwayScoreHomeTeamGiven:
		case HomeAndAwayScoreAwayTeamTaken:
		case HomeScoreHomeTeamAgainstTeamLevelGiven:
		case HomeScoreAwayTeamAgainstTeamLevelTaken:
		case HomeAndAwayScoreHomeTeamAgainstTeamLevelGiven:
		case HomeAndAwayScoreAwayTeamAgainstTeamLevelTaken:
			return true;
		case AwayScoreAwayTeamGiven:
		case AwayScoreHomeTeamTaken:
		case HomeAndAwayScoreAwayTeamGiven:
		case HomeAndAwayScoreHomeTeamTaken:
		case AwayScoreAwayTeamAgainstTeamLevelGiven:
		case AwayScoreHomeTeamAgainstTeamLevelTaken:
		case HomeAndAwayScoreAwayTeanAgainstTeamLevelGiven:
		case HomeAndAwayScoreHomeTeamAgainstTeamLevelTaken:
			return false;
		}
		
		throw new RuntimeException("Not a ScoreType enum instance");
	}
}
