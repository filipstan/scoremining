package com.sm.fpe.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sm.fpe.model.Competition;
import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.model.Team;

public interface MatchUnplayedDao extends JpaRepository<MatchUnplayed, Integer>, MatchUnplayedCustomDao {
	public List<MatchUnplayed> findByCompetitionAndHomeTeamAndAwayTeamAndTimestamp(Competition competition,
			Team homeTeam, Team awayTeam, Timestamp timestamp);

	public List<MatchUnplayed> findByHomeTeamOrAwayTeam(Team homeTeam, Team awayTeam);

	@Query("SELECT m FROM MatchUnplayed m GROUP BY m.timestamp, m.awayTeam, m.competition, m.homeTeam HAVING COUNT(m) > 1")
	public List<MatchUnplayed> findDuplicates();

	public List<MatchUnplayed> findByTimestampBetween(Timestamp start, Timestamp end);
}
