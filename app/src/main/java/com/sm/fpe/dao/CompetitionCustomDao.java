package com.sm.fpe.dao;

import com.sm.fpe.model.Competition;

public interface CompetitionCustomDao {
	public Competition saveIfNotExists(Competition competition);
}
