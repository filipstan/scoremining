package com.sm.fpe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sm.fpe.model.Competition;

public class CompetitionDaoImpl implements CompetitionCustomDao {

	@Autowired
	private CompetitionDao competitionDao;
	
	@Override
	public Competition saveIfNotExists(Competition competition) {
		List<Competition> competitions = competitionDao.findByNameAndCountry(competition.getName(), competition.getCountry());
		if (competitions.isEmpty()){
			return competitionDao.save(competition);
		} else {
			return competitions.get(0);
		}
	}

}
