package com.sm.fpe.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sm.fpe.model.Country;

public interface CountryDao extends JpaRepository<Country, Integer>, CountryCustomDao {
	public List<Country> findByName(String name);
}
