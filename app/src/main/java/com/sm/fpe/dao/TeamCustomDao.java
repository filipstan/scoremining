package com.sm.fpe.dao;

import com.sm.fpe.model.Team;

public interface TeamCustomDao {
	public Team saveIfNotExists(Team team);
}
