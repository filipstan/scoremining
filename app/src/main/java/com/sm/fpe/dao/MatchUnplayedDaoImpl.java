package com.sm.fpe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sm.fpe.model.MatchUnplayed;

public class MatchUnplayedDaoImpl implements MatchUnplayedCustomDao {

	@Autowired
	private MatchUnplayedDao matchUnplayedDao;

	@Override
	public MatchUnplayed saveIfNotExists(MatchUnplayed match) {
		List<MatchUnplayed> matches = matchUnplayedDao.findByCompetitionAndHomeTeamAndAwayTeamAndTimestamp(
				match.getCompetition(), match.getHomeTeam(), match.getAwayTeam(), match.getTimestamp());
		if (matches.isEmpty()) {
			return matchUnplayedDao.save(match);
		} else {
			return matches.get(0);
		}
	}

}
