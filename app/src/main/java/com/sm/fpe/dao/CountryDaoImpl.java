package com.sm.fpe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sm.fpe.model.Country;

public class CountryDaoImpl implements CountryCustomDao {

	@Autowired
	private CountryDao countryDao;

	public Country saveIfNotExists(Country country) {
		List<Country> countries = countryDao.findByName(country.getName());
		if (countries.isEmpty()) {
			return countryDao.save(country);
		} else {
			return countries.get(0);
		}
	}

}
