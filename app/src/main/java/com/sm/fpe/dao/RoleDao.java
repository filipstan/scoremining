package com.sm.fpe.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sm.fpe.model.Role;

public interface RoleDao extends JpaRepository<Role, Integer> {
}
