package com.sm.fpe.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sm.fpe.model.Notification;

public interface NotificationDao extends JpaRepository<Notification, Integer>, NotificationCustomDao {
	@Query("SELECT n FROM Notification n WHERE n.id < ?1 ORDER BY n.id DESC")
	public List<Notification> findByIdLessThanOrderByIdDesc(int id, Pageable pageable);
	
	@Query("SELECT n FROM Notification n ORDER BY n.id DESC")
	public List<Notification> findOrderByIdDesc(Pageable pageable);
}
