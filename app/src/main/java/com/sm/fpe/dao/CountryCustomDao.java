package com.sm.fpe.dao;

import com.sm.fpe.model.Country;

public interface CountryCustomDao {
	public Country saveIfNotExists(Country country);
}
