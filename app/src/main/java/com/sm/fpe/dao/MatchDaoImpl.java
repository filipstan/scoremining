package com.sm.fpe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sm.fpe.model.Match;

public class MatchDaoImpl implements MatchCustomDao {

	@Autowired
	private MatchDao matchDao;

	public Match saveIfNotExists(Match match) {
		List<Match> matches = matchDao.findByHomeTeamAndAwayTeamAndCompetitionAndHomeScoreAndAwayScoreAndTimestamp(
				match.getHomeTeam(), match.getAwayTeam(), match.getCompetition(), match.getHomeScore(),
				match.getAwayScore(), match.getTimestamp());
		if (matches.isEmpty()) {
//			System.out.println(match);
			return matchDao.save(match);
		} else {
			return matches.get(0);
		}
	}

}
