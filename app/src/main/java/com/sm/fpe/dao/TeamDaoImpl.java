package com.sm.fpe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sm.fpe.model.Team;

public class TeamDaoImpl implements TeamCustomDao {

	@Autowired
	private TeamDao teamDao;

	public Team saveIfNotExists(Team team) {
		List<Team> teams = teamDao.findByNameAndCountry(team.getName(), team.getCountry());
		if (teams.isEmpty()) {
			return teamDao.save(team);
		} else {
			return teams.get(0);
		}
	}

}
