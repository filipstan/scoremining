package com.sm.fpe.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sm.fpe.model.Competition;
import com.sm.fpe.model.Country;

public interface CompetitionDao extends JpaRepository<Competition, Integer>, CompetitionCustomDao {
	public List<Competition> findByNameAndCountry(String name, Country country);
	public List<Competition> findAllByOrderByCountry_NameAsc();
}
