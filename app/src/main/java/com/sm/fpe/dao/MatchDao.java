package com.sm.fpe.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sm.fpe.model.Competition;
import com.sm.fpe.model.Match;
import com.sm.fpe.model.Team;
import com.sm.fpe.model.TeamLevel;

public interface MatchDao extends JpaRepository<Match, Integer>, MatchCustomDao {

	public List<Match> findByHomeTeamAndAwayTeamAndCompetitionAndHomeScoreAndAwayScoreAndTimestamp(Team homeTeam,
			Team awayTeam, Competition competition, int homeScore, int awayScore, Timestamp timestamp);

//	@Query("SELECT m FROM (SELECT _m FROM Match _m WHERE _m.homeTeam = ?1 ORDER BY _m.timestamp DESC LIMIT 10) ORDER BY m.timestamp ASC")
	public List<Match> findByHomeTeam(Team team);

	public List<Match> findByAwayTeam(Team team);

	public List<Match> findByHomeTeamAndAwayTeam_Level(Team team, TeamLevel level);

	public List<Match> findByAwayTeamAndHomeTeam_Level(Team team, TeamLevel level);

	public List<Match> findByHomeTeamOrAwayTeam(Team homeTeam, Team awayTeam);

	public List<Match> findByHomeTeamAndAwayTeam_LevelOrAwayTeamAndHomeTeam_Level(Team homeTeam, TeamLevel awayLevel,
			Team awayTeam, TeamLevel homeLevel);

	public List<Match> findByHomeTeam_LevelAndAwayTeam_Level(TeamLevel homeTeamLevel, TeamLevel awayTeamLevel);

	public List<Match> findByCompetitionAndHomeTeamAndAwayTeamAndTimestamp(Competition competition, Team homeTeam,
			Team awayTeam, Timestamp timestamp);

	public List<Match> findByTimestampBetween(Timestamp start, Timestamp end);

	// SELECT * FROM `peso_match` GROUP BY `AWAY_SCORE`, `HOME_SCORE`, `DATE`,
	// `AWAY_TEAM_ID`, `COMPETITION_ID`, `HOME_TEAM_ID` HAVING COUNT(*) > 1
	@Query("SELECT m FROM Match m GROUP BY m.awayScore, m.homeScore, m.timestamp, m.awayTeam, m.competition, m.homeTeam HAVING COUNT(m) > 1")
	public List<Match> findDuplicates();
}
