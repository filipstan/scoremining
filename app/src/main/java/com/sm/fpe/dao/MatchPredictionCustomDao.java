package com.sm.fpe.dao;

import com.sm.fpe.model.MatchPrediction;

public interface MatchPredictionCustomDao {
	public MatchPrediction saveIfNotExists(MatchPrediction matchPrediction);
}
