package com.sm.fpe.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sm.fpe.model.Country;
import com.sm.fpe.model.Team;
import com.sm.fpe.model.TeamLevel;

public interface TeamDao extends JpaRepository<Team, Integer>, TeamCustomDao {

	public List<Team> findByName(String name);
	
//	public List<Team> findByNameAndCountry_Name(String name, String countryName);

	public List<Team> findByNameAndCountry(String name, Country country);

	public List<Team> findByLevel(TeamLevel level);

	// SELECT *, COUNT(`NAME`) FROM `peso_team` GROUP BY `NAME` HAVING
	// COUNT(`NAME`) > 1
	@Query("SELECT t FROM Team t GROUP BY t.name HAVING count(t) > 1")
	public List<Team> findDuplicates();
	
	@Query("SELECT t FROM Team t WHERE t.name IN (SELECT _t.name FROM Team _t GROUP BY _t.name HAVING count(_t) > 1)")
	public List<Team> findAllDuplicatesByName();
}
