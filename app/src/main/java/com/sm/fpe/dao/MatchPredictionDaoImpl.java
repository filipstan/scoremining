package com.sm.fpe.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sm.fpe.model.MatchPrediction;

public class MatchPredictionDaoImpl implements MatchPredictionCustomDao {

	@Autowired
	private MatchPredictionDao matchPredictionDao;

	public MatchPrediction saveIfNotExists(MatchPrediction matchPrediction) {
		List<MatchPrediction> matchPredictions = matchPredictionDao.findByScoreAndScoreTypeAndMatchUnplayedAndTrustAndProfile(
				matchPrediction.getScore(), matchPrediction.getScoreType(), matchPrediction.getMatchUnplayed(),
				matchPrediction.getTrust(), matchPrediction.getProfile());
		if (matchPredictions.isEmpty()) {
			return matchPredictionDao.save(matchPrediction);
		} else {
			return matchPredictions.get(0);
		}
	}
}
