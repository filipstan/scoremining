package com.sm.fpe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sm.fpe.model.User;

public interface UserDao extends JpaRepository<User, Integer> {

	public User findByEmail(String email);
}
