package com.sm.fpe.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sm.fpe.ScoreType;
import com.sm.fpe.model.MatchPrediction;
import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.model.Profile;

public interface MatchPredictionDao extends JpaRepository<MatchPrediction, Integer>, MatchPredictionCustomDao {

	public List<MatchPrediction> findByScoreAndScoreTypeAndMatchUnplayedAndTrustAndProfile(int score, ScoreType scoreType,
			MatchUnplayed matchUnplayed, int trust, Profile profile);

	public List<MatchPrediction> findByMatchUnplayedAndScoreTypeOrderByTrustDesc(MatchUnplayed matchUnplayed,
			ScoreType scoreType);

	public List<MatchPrediction> findByMatchUnplayed(MatchUnplayed matchUnplayed);
	
	public List<MatchPrediction> findByMatchUnplayedAndProfile(MatchUnplayed matchUnplayed, Profile profile);
	
	@Query("SELECT mp FROM MatchPrediction mp WHERE mp.matchUnplayed.timestamp BETWEEN ?1 AND ?2")
	public List<MatchPrediction> findByTimestampBetween(Timestamp start, Timestamp end);
	
	@Query("SELECT mp FROM MatchPrediction mp WHERE mp.matchUnplayed.timestamp BETWEEN ?1 AND ?2 GROUP BY matchUnplayed")
	public List<MatchPrediction> findMatchesUnplayedByTimestampBetween(Timestamp fromTt, Timestamp toTt);
	
	@Query("SELECT mp FROM MatchPrediction mp WHERE matchUnplayed = :mu GROUP BY profile")
	public List<MatchPrediction> findProfilesByMatchUnplayed(@Param("mu") MatchUnplayed mu);
}
