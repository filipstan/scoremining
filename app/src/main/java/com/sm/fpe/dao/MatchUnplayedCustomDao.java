package com.sm.fpe.dao;

import com.sm.fpe.model.MatchUnplayed;

public interface MatchUnplayedCustomDao {
	public MatchUnplayed saveIfNotExists(MatchUnplayed match);
}
