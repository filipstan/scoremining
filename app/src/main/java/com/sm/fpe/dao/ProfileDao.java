package com.sm.fpe.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sm.fpe.model.Profile;

public interface ProfileDao extends JpaRepository<Profile, Integer> {
}
