package com.sm.fpe.dao;

import com.sm.fpe.model.Match;

public interface MatchCustomDao {
	public Match saveIfNotExists(Match match);
}
