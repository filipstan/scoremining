package com.sm.fpe;

public enum StrategyType {

	MostFrequentScore, BiggestTrustSum
}
