package com.sm.fpe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Transaction<T extends Comparable<T>> implements Comparable<Transaction<T>> {
	private List<Item<T>> list;
	private Timestamp date;
	
	public Transaction(Timestamp date) {
		super();
		this.setDate(date);
		this.list = new ArrayList<>();
	}
	
	public Transaction(List<Item<T>> list, Timestamp date) {
		super();
		this.setDate(date);
		this.setList(list);
		Collections.sort(this.list);
	}

	public Transaction(Item<T> item, Timestamp date) {
		super();
		this.setDate(date);
		this.list = new ArrayList<>();
		this.list.add(item);
		this.setList(list);
	}

	public List<Item<T>> getList() {
		return list;
	}

	public void setList(List<Item<T>> list) {
		this.list = list;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int compareTo(Transaction<T> o) {
		return this.date.compareTo(o.getDate());
	}

	public void add(Item<T> item) {
		this.list.add(item);
	}
	
	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction<T> other = (Transaction<T>) obj;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Item<T> item : this.list) {
			sb.append(item + ", ");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		return "(" + sb + ")";
		// return "(" + sb + "; " + date + ")";
		// return "\n\t\tTransaction [list=" + list + ", date=" + date + "]";
	}

}
