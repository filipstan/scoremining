package com.sm.fpe;

import org.hibernate.cfg.DefaultNamingStrategy;

public class CustomNamingStrategy extends DefaultNamingStrategy {
	private static final long serialVersionUID = -6009909616338080385L;

	@Override
	public String tableName(String tableName) {
		return "PESO_" + super.tableName(tableName);
	}
}
