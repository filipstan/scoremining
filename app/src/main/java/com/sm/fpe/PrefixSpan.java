package com.sm.fpe;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sm.fpe.service.SequenceMiningService;

@Component
@Scope("prototype")
public class PrefixSpan implements Runnable {

	private List<Sequence<Integer>> seqDb;
	private Sequence<Integer> prefix;
	private long maxGap;
	private CountDownLatch latch;

	public PrefixSpan(List<Sequence<Integer>> seqDb, Sequence<Integer> prefix, long maxGap, CountDownLatch latch) {
		super();
		this.seqDb = seqDb;
		this.prefix = prefix;
		this.maxGap = maxGap;
		this.latch = latch;
	}

	// @Autowired
	// private ApplicationContext context;

	@Autowired
	private SequenceMiningService smService;

	// @Autowired
	// private ThreadPoolTaskExecutor taskExecutor;

	@Override
	public void run() {
		// System.out.println("Started thread for " + this.prefix);
		this.run(this.seqDb, this.prefix);
		// System.out.println("Finished thread for " + this.prefix);

		latch.countDown();
		// System.out.println(latch + " countDown");
	}

	public void run(List<Sequence<Integer>> seqDb, Sequence<Integer> prefix) {

		smService.addPrefix(prefix);

		// System.out.println("prefix " + this.prefix + " " + prefix);

		Map<Item<Integer>, Integer> itemFrequencies = smService.filterItems(seqDb);
		Sequence<Integer> newPrefix;
		long seconds;

		List<Sequence<Integer>> newSeqDb;
		Sequence<Integer> newSeq;

		for (Item<Integer> _item : itemFrequencies.keySet()) {

			newSeqDb = new ArrayList<>();

			for (Sequence<Integer> _seq : seqDb) {
				newSeq = _seq.getSequenceAfterItem(_item);
				
				if (!newSeq.isEmpty()) {
					
					seconds = newSeq.getStartTimestamp().getTime() - _seq.getStartTimestamp().getTime();
					seconds /= 1000;
					
					if (seconds <= this.maxGap) {
						newSeqDb.add(newSeq);
					}
				}
			}

			newPrefix = prefix.copySequence();
			newPrefix.add(new Item<>(_item.getValue()), null);
			newPrefix.setFrequency(itemFrequencies.get(_item));

			run(newSeqDb, newPrefix);
		}
	}
}
