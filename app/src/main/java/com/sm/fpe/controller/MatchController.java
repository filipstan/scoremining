package com.sm.fpe.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.sm.fpe.service.DataUtils;
import com.sm.fpe.service.MatchService;
import com.sm.fpe.service.MatchUnplayedService;
import com.sm.fpe.service.NotificationService;

@Controller
public class MatchController {

	@Autowired
	MatchService matchService;

	@Autowired
	MatchUnplayedService matchUnplayedService;


	@Autowired
	DataUtils dataUtils;

	@Autowired
	private NotificationService notificationService;

	@RequestMapping("/admin/matches/")
	public ModelAndView matches() {

		ModelAndView model = new ModelAndView();

		model.setViewName("matches");
		return model;
	}

	@RequestMapping("/admin/getMatchesPlayed")
	@ResponseBody
	public String getMatchesPlayed(@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate) throws ParseException {
		return matchService.toJSON(matchService.getMatches(fromDate, toDate));
	}

	@RequestMapping("/admin/getMatchesUnplayed")
	@ResponseBody
	public String getMatchesUnplayed(@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate) throws ParseException {
		return matchUnplayedService.toJSON(matchUnplayedService.getUnplayedMatches(fromDate, toDate));
	}

	@RequestMapping("/admin/listMpFiles")
	@ResponseBody
	public String listMpFiles() {
		return dataUtils.toJSON(dataUtils.listMpFiles());
	}

	@RequestMapping("/admin/listMuFiles")
	@ResponseBody
	public String listMuFiles() {
		return dataUtils.toJSON(dataUtils.listMuFiles());
	}

	@RequestMapping(value = "/admin/saveFileMpToDb", method = RequestMethod.POST)
	@ResponseBody
	public String saveFileMpToDb(@RequestParam List<String> filenames) {

		int nbrMatchesSaved;
		String res, ret = "";

		for (String filename : filenames) {
			nbrMatchesSaved = dataUtils.saveFileToDb(filename, false);
			res = "File: " + filename + " saved with " + nbrMatchesSaved + " matches. ";
			notificationService.createAndSendNotification(res);
			ret += res;
		}

		return ret;
	}

	@RequestMapping(value = "/admin/saveFileMuToDb", method = RequestMethod.POST)
	@ResponseBody
	public String saveFileMuToDb(@RequestParam List<String> filenames) {

		int nbrMatchesSaved;
		String res, ret = "";

		for (String filename : filenames) {
			nbrMatchesSaved = dataUtils.saveFileToDb(filename, true);
			res = "File: " + filename + " saved with " + nbrMatchesSaved + " unplayed matches. ";
			notificationService.createAndSendNotification(res);
			ret += res;
		}

		return ret;
	}

	@RequestMapping(value = "/admin/callCrawler", method = RequestMethod.POST)
	@ResponseBody
	public String callCrawler(@RequestParam(value = "callCrawlerDate") String date) throws ParseException, IOException {

		String ret = "";
		int days, notificationId;
		JSONObject obj;

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		days = (int) ((formatter.parse(date).getTime() - calendar.getTimeInMillis()) / (1000 * 60 * 60 * 24));

		RestTemplate restTemplate = new RestTemplate();
		notificationId = notificationService.createAndSendNotification("Starting crawler for " + date);
		String crawlerUrl = "http://localhost:3000/callCrawler?nbrDays=" + days + "&notificationId=" + notificationId;
		ResponseEntity<JSONObject> response = restTemplate.getForEntity(crawlerUrl, JSONObject.class);
		obj = response.getBody();
		ret = obj.get("msg").toString();
		notificationService.createAndSendNotification(ret);

		return ret;
	}

}
