package com.sm.fpe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sm.fpe.service.NotificationService;
import com.sm.fpe.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping("/admin/users/")
	public ModelAndView users() {

		ModelAndView model = new ModelAndView();

		model.addObject("users", userService.getAllUsers());

		model.setViewName("users");

		return model;
	}

}
