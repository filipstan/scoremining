package com.sm.fpe.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.sm.fpe.service.NotificationService;

@Controller
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@MessageMapping("/admin/notification/getLast/ws")
	@SendTo("/topic/messages")
	public String handle(String obj) {

		JSONParser parser = new JSONParser();
		JSONObject jsonObj;
		int beforeId = 0, limit = 5;

		try {
			jsonObj = (JSONObject) parser.parse(obj);
			limit = Integer.parseInt((String) jsonObj.get("limit"));
			beforeId = Integer.parseInt((String) jsonObj.get("beforeId"));
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}

		return notificationService.toJSON(notificationService.getLast(beforeId, limit));
	}
}
