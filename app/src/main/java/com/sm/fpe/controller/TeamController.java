package com.sm.fpe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sm.fpe.service.DataUtils;
import com.sm.fpe.service.NotificationService;
import com.sm.fpe.service.TeamService;

@Controller
public class TeamController {

	@Autowired
	TeamService teamService;

	@Autowired
	DataUtils dataUtils;

	@Autowired
	NotificationService notificationService;

	@RequestMapping("/admin/teams/")
	public ModelAndView teams() {

		ModelAndView model = new ModelAndView();

		model.setViewName("teams");
		return model;
	}

	@RequestMapping("/admin/getAllTeamsJSON")
	@ResponseBody
	public String getAllTeamsJSON() {
		return teamService.toJSON(teamService.getAllTeams());
	}

	@RequestMapping("/admin/getDuplicateTeamsJSON")
	@ResponseBody
	public String getDuplicateTeamsJSON() {
		return teamService.toJSON(teamService.getDuplicateTeams());
	}

	@RequestMapping(value = "/admin/mergeTeams", method = RequestMethod.POST)
	@ResponseBody
	public String mergeTeams(@RequestParam int keepTeamId, @RequestParam int removeTeamId) {
		String res = teamService.mergeTeams(keepTeamId, removeTeamId);
		res = "[Merge Teams]: " + res;
		notificationService.createAndSendNotification(res);
		return res;
	}

	@RequestMapping(value = "/admin/mergeAllTeams", method = RequestMethod.GET)
	@ResponseBody
	public String mergeAllTeams() {
		String res = teamService.mergeAllTeams();
		res = "[Merge All Teams]: " + res;
		notificationService.createAndSendNotification(res);
		return res;
	}

	@RequestMapping(value = "/admin/classifyTeams", method = RequestMethod.GET)
	@ResponseBody
	public String classifyTeams() {
		String res = "All teams were classified";
		dataUtils.classifyTeams();
		notificationService.createAndSendNotification(res);
		return res;
	}

}
