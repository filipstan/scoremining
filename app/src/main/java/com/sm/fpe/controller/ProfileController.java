package com.sm.fpe.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sm.fpe.ScoreType;
import com.sm.fpe.StrategyType;
import com.sm.fpe.model.Competition;
import com.sm.fpe.model.Profile;
import com.sm.fpe.service.CompetitionService;
import com.sm.fpe.service.NotificationService;
import com.sm.fpe.service.ProfileService;

@Controller
public class ProfileController {

	@Autowired
	ProfileService profileService;

	@Autowired
	CompetitionService competitionService;

	@Autowired
	private NotificationService notificationService;

	@RequestMapping("/admin/profiles/")
	public ModelAndView profiles() {

		ModelAndView model = new ModelAndView();

		model.addObject("scoreTypes", ScoreType.values());
		model.addObject("strategyTypes", StrategyType.values());

		model.setViewName("profiles");

		return model;
	}

	@RequestMapping("/admin/profiles/getAllProfiles/")
	@ResponseBody
	public String getAllProfiles(@RequestParam(value = "id", required = false) String profileId) {

		if (profileId != null && !profileId.isEmpty()) {
			int id = Integer.parseInt(profileId);
			return profileService.toJSON(Arrays.asList(profileService.getProfileById(id)));
		}

		return profileService.toJSON(profileService.getAllProfiles());
	}

	@RequestMapping(value = "/admin/profiles/newProfile/", method = RequestMethod.POST)
	@ResponseBody
	public String newProfile(@RequestParam String name, @RequestParam int minSup, @RequestParam int maxGap,
			@RequestParam int sequenceMaxLength, @RequestParam List<Integer> competitions,
			@RequestParam List<ScoreType> scoreTypes, @RequestParam StrategyType strategyType,
			@RequestParam String trustFormula) {

		Profile profile = new Profile();
		String res;
		Collection<Competition> competitionsIds;
		
		competitionsIds = new ArrayList<>();
		
		for (int id : competitions) {
			competitionsIds.add(new Competition(id));
		}
		

		profile.setName(name);
		profile.setMinSup(minSup);
		profile.setMaxGap(maxGap);
		profile.setSequenceMaxLength(sequenceMaxLength);
		profile.setCompetitions(competitionsIds);
		profile.setScoreTypes(scoreTypes);
		profile.setStrategyType(strategyType);
		profile.setTrustFormula(trustFormula);

		profileService.save(profile);

		res = "Profile " + name + " saved";

		notificationService.createAndSendNotification(res);

		return res;
	}

	@RequestMapping("/admin/profiles/removeProfile/")
	@ResponseBody
	public String getRemoveProfile(@RequestParam(value = "id", required = true) String profileId) {

		profileService.removeProfile(Integer.parseInt(profileId));

		return "";
	}

	@RequestMapping("/admin/profiles/getAllCompetitions")
	@ResponseBody
	public String getAllCompetitions(@RequestParam(value = "id", required = false) String profileId) {

		if (profileId != null && !profileId.isEmpty()) {
			// int id = Integer.parseInt(profileId);
			// return
			// profileService.toJSON(Arrays.asList(profileService.getProfileById(id)));
		}

		return competitionService.toJSON(competitionService.getAllCompetitions());
	}
}
