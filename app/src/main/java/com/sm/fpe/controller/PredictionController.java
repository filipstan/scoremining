package com.sm.fpe.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.service.DataUtils;
import com.sm.fpe.service.NotificationService;
import com.sm.fpe.service.PredictionService;

@Controller
public class PredictionController {

	@Autowired
	PredictionService predictionService;

	@Autowired
	DataUtils dataUtils;

	@Autowired
	private NotificationService notificationService;

	@RequestMapping("/admin/getMatchesPredictions/")
	@ResponseBody
	public String getMatchesPredictions(@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate) throws ParseException {
		return predictionService.toJSON(predictionService.getMatchesPredictions(fromDate, toDate));
	}
	
	@RequestMapping("/admin/deleteMatchesPredictions/")
	@ResponseBody
	public String deleteMatchesPredictions(@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate) throws ParseException {
		String res;
		
		res = predictionService.deleteMatchesPredictions(fromDate, toDate);
		if (res != "") {
			notificationService.createAndSendNotification(res);
		}
		
		return res;
	}

	@RequestMapping("/admin/getMatchesPredictionsGrouped/")
	@ResponseBody
	public String getMatchesPredictionsGrouped(@RequestParam(value = "fromDate", required = false) String fromDate,
			@RequestParam(value = "toDate", required = false) String toDate,
			@RequestParam(value = "onlyTrusty", required = false) String onlyTrusty) throws ParseException {
		return predictionService.getMatchesPredictionsGroupedJSON(fromDate, toDate, onlyTrusty);
	}

	@RequestMapping("/admin/predictions/")
	public ModelAndView predictions() {

		ModelAndView model = new ModelAndView();

		model.setViewName("predictions");

		return model;
	}

	@RequestMapping("/admin/predictions/getPredictionsMu/")
	@ResponseBody
	public String getPredictionsMu(@RequestParam int[] matches, @RequestParam int profile) {
		
		String res;

		for (int i = 0; i < matches.length; i++) {
			res = predictionService.computePrediction(matches[i], profile);
			notificationService.createAndSendNotification(res);
		}

		return "Done";
	}

	@RequestMapping("/admin/predictions/getPredictionsMp/")
	@ResponseBody
	public String getPredictionsMp(@RequestParam int[] matches, @RequestParam int profile) {

		MatchUnplayed mu;
		String res;

		for (int i = 0; i < matches.length; i++) {
			mu = dataUtils.convertMatchPlayedToUnplayed(matches[i]);
			res = predictionService.computePrediction(mu.getId(), profile);
			notificationService.createAndSendNotification(res);
		}

		return "Done";
	}
}
