package com.sm.fpe.service;

import java.security.InvalidParameterException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.sm.fpe.Item;
import com.sm.fpe.PrefixSpan;
import com.sm.fpe.ScoreType;
import com.sm.fpe.Sequence;
import com.sm.fpe.Transaction;
import com.sm.fpe.dao.MatchDao;
import com.sm.fpe.dao.TeamDao;
import com.sm.fpe.model.Match;
import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.model.Profile;
import com.sm.fpe.model.Team;
import com.sm.fpe.model.TeamLevel;

@Service
public class SequenceMiningService {

	private List<Sequence<Integer>> seqDb = null;
	private double minSup = 0.5;
	private int minSupAbs;
	private long maxGap;
	private List<Sequence<Integer>> resPrefixes;
	private Set<Item<Integer>> frequentItems;

	@Autowired
	private ApplicationContext context;

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private PredictionService predictionService;

	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	public List<Sequence<Integer>> getSeqDb() {
		return seqDb;
	}

	public void setSeqDb(List<Sequence<Integer>> seqDb) {
		this.seqDb = seqDb;
	}

	public double getMinSup() {
		return this.minSup;
	}

	public void setMinSup(double minSup) {
		if (minSup < 0) {
			throw new InvalidParameterException("Invalid minSup value");
		}
		this.minSup = minSup;
	}

	public long getMaxGap() {
		return maxGap;
	}

	public void setMaxGap(long maxGap) {
		this.maxGap = maxGap;
	}

	public List<Sequence<Integer>> getResPrefixes() {
		return resPrefixes;
	}

	public void setResPrefixes(List<Sequence<Integer>> resPrefixes) {
		this.resPrefixes = resPrefixes;
	}
	
	public void addPrefix(Sequence<Integer> prefix) {
		this.resPrefixes.add(prefix);
	}

	public Set<Item<Integer>> getFrequentItems() {
		return frequentItems;
	}

	public void setFrequentItems(Set<Item<Integer>> frequentItems) {
		this.frequentItems = frequentItems;
	}

	public void printPrefixes(List<Sequence<Integer>> prefixes) {
		System.out.println("Prefixes: [");
		prefixes.forEach(_sequence -> {
			System.out.println(_sequence.printPrefix());
		});
		System.out.println("]");
	}

	// call before filterItems
	public void convertMinSupToAbsoluteValue() {
		if (this.minSup < 1) {
			this.minSupAbs = (int) (this.minSup * this.seqDb.size());
			if (this.minSupAbs < 1) {
				this.minSupAbs = 1;
			}
		} else {
			this.minSupAbs = (int) this.minSup;
		}
	}

	public List<Sequence<Integer>> getSequences(MatchUnplayed matchUnplayed, ScoreType scoreType, Profile profile) {

		List<Sequence<Integer>> sequences = new ArrayList<>();
		Team team;
		TeamLevel opponentTeamLevel;

		team = predictionService.getTeamByScoreType(matchUnplayed, scoreType);
		opponentTeamLevel = (matchUnplayed.getHomeTeam() == team ? matchUnplayed.getAwayTeam().getLevel()
				: matchUnplayed.getHomeTeam().getLevel());

		for (Team _team : teamDao.findByLevel(team.getLevel())) {

			Sequence<Integer> s = this.getSequence(_team, opponentTeamLevel, scoreType, matchUnplayed.getTimestamp(),
					profile.getSequenceMaxLength());

			if (!s.isEmpty()) {
				sequences.add(s);
			}
		}

		return sequences;
	}

	public Sequence<Integer> getSequence(MatchUnplayed matchUnplayed, ScoreType scoreType) {

		Team team = predictionService.getTeamByScoreType(matchUnplayed, scoreType);
		TeamLevel opponentTeamLevel = (matchUnplayed.getHomeTeam() == team ? matchUnplayed.getAwayTeam().getLevel()
				: matchUnplayed.getHomeTeam().getLevel());

		return getSequence(team, opponentTeamLevel, scoreType, matchUnplayed.getTimestamp(), 0);
	}

	private Sequence<Integer> getSequence(Team team, TeamLevel opponentTeamLevel, ScoreType scoreType,
			Timestamp timestamp, int sequenceMaxLength) {

		Sequence<Integer> sequence = new Sequence<>();

		switch (scoreType) {
		case HomeScoreHomeTeamGiven:
			for (Match _match : matchDao.findByHomeTeam(team)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getHomeScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case AwayScoreAwayTeamGiven:
			for (Match _match : matchDao.findByAwayTeam(team)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getAwayScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case HomeScoreAwayTeamTaken:
			for (Match _match : matchDao.findByAwayTeam(team)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getHomeScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case AwayScoreHomeTeamTaken:
			for (Match _match : matchDao.findByHomeTeam(team)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getAwayScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case HomeAndAwayScoreHomeTeamGiven:
		case HomeAndAwayScoreAwayTeamGiven:
			for (Match _match : matchDao.findByHomeTeamOrAwayTeam(team, team)) {
				int score;
				if (_match.getHomeTeam().equals(team)) {
					score = _match.getHomeScore();
				} else {
					score = _match.getAwayScore();
				}
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(score), _match.getTimestamp());
				sequence.add(t);
			}
			break;
		case HomeAndAwayScoreHomeTeamTaken:
		case HomeAndAwayScoreAwayTeamTaken:
			for (Match _match : matchDao.findByHomeTeamOrAwayTeam(team, team)) {
				int score;
				if (_match.getHomeTeam().equals(team)) {
					score = _match.getAwayScore();
				} else {
					score = _match.getHomeScore();
				}
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(score), _match.getTimestamp());
				sequence.add(t);
			}
			break;

		case HomeScoreHomeTeamAgainstTeamLevelGiven:
			for (Match _match : matchDao.findByHomeTeamAndAwayTeam_Level(team, opponentTeamLevel)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getHomeScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case AwayScoreAwayTeamAgainstTeamLevelGiven:
			for (Match _match : matchDao.findByAwayTeamAndHomeTeam_Level(team, opponentTeamLevel)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getAwayScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case HomeScoreAwayTeamAgainstTeamLevelTaken:
			for (Match _match : matchDao.findByAwayTeamAndHomeTeam_Level(team, opponentTeamLevel)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getHomeScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case AwayScoreHomeTeamAgainstTeamLevelTaken:
			for (Match _match : matchDao.findByHomeTeamAndAwayTeam_Level(team, opponentTeamLevel)) {
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(_match.getAwayScore()),
						_match.getTimestamp());
				sequence.add(t);
			}
			break;
		case HomeAndAwayScoreHomeTeamAgainstTeamLevelGiven:
		case HomeAndAwayScoreAwayTeanAgainstTeamLevelGiven:
			for (Match _match : matchDao.findByHomeTeamAndAwayTeam_LevelOrAwayTeamAndHomeTeam_Level(team,
					opponentTeamLevel, team, opponentTeamLevel)) {
				int score;
				if (_match.getHomeTeam().equals(team)) {
					score = _match.getHomeScore();
				} else {
					score = _match.getAwayScore();
				}
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(score), _match.getTimestamp());
				sequence.add(t);
			}
			break;
		case HomeAndAwayScoreHomeTeamAgainstTeamLevelTaken:
		case HomeAndAwayScoreAwayTeamAgainstTeamLevelTaken:
			for (Match _match : matchDao.findByHomeTeamAndAwayTeam_LevelOrAwayTeamAndHomeTeam_Level(team,
					opponentTeamLevel, team, opponentTeamLevel)) {
				int score;
				if (_match.getHomeTeam().equals(team)) {
					score = _match.getAwayScore();
				} else {
					score = _match.getHomeScore();
				}
				Transaction<Integer> t = new Transaction<>(new Item<Integer>(score), _match.getTimestamp());
				sequence.add(t);
			}
			break;
		}

		sequence.orderSequence();
		sequence.removeTransactionsAfterTimestamp(timestamp);
		sequence.keepLastTransactions(sequenceMaxLength);

		return sequence;
	}

	public void runPrefixSpan(List<Sequence<Integer>> seqDb) {
		setResPrefixes(Collections.synchronizedList(new ArrayList<>()));
		setSeqDb(seqDb);
		convertMinSupToAbsoluteValue();
		runPrefixSpan();
	}

	private void runPrefixSpan() {

		long startTime, stopTime, elapsedTime;

		startTime = System.currentTimeMillis();

		List<Sequence<Integer>> seqDb = this.getSeqDb();
		Sequence<Integer> newPrefix;
		CountDownLatch latch;
		List<Sequence<Integer>> newSeqDb;
		Sequence<Integer> newSeq;
		Map<Item<Integer>, Integer> itemFrequencies;

		itemFrequencies = filterItems(seqDb);

		this.setFrequentItems(itemFrequencies.keySet());

		latch = new CountDownLatch(itemFrequencies.size());

		for (Item<Integer> _item : itemFrequencies.keySet()) {

			newSeqDb = new ArrayList<>();

			for (Sequence<Integer> _seq : seqDb) {
				newSeq = _seq.getSequenceAfterItem(_item);
				if (!newSeq.isEmpty()) {
					newSeqDb.add(newSeq);
				}
			}

			newPrefix = new Sequence<>();
			newPrefix.add(new Item<>(_item.getValue()), null);
			newPrefix.setFrequency(itemFrequencies.get(_item));

			PrefixSpan psThread = (PrefixSpan) context.getBean("prefixSpan", newSeqDb, newPrefix, this.getMaxGap(), latch);
			taskExecutor.execute(psThread);
		}

		try {
			latch.await();
		} catch (InterruptedException e) {
			System.out.println(e);
		}

		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Run PrefixSpan " + taskExecutor.getCorePoolSize() + " threads " + "[" + elapsedTime
				+ " ms]." + " Was started in parallel " + itemFrequencies.size() + " threads." + " Database has "
				+ seqDb.size() + " sequences.");

	}

	private Map<Item<Integer>, Integer> countItems(List<Sequence<Integer>> seqDb) {

		Map<Item<Integer>, Integer> itemFrequencies = new HashMap<>();

		seqDb.forEach(_sequence -> {

			Map<Item<Integer>, Integer> _seqItemFrequencies = new HashMap<>();

			_sequence.getList().forEach(_transaction -> {
				_transaction.getList().forEach(_item -> {
					_seqItemFrequencies.put(_item, 1);
				});
			});

			_seqItemFrequencies.keySet().forEach(_item -> {
				if (itemFrequencies.get(_item) != null) {
					itemFrequencies.put(_item, itemFrequencies.get(_item) + _seqItemFrequencies.get(_item));
				} else {
					itemFrequencies.put(_item, _seqItemFrequencies.get(_item));
				}
			});
		});

		return itemFrequencies;
	}

	public Map<Item<Integer>, Integer> filterItems(List<Sequence<Integer>> seqDb) {

		Map<Item<Integer>, Integer> itemFrequencies = countItems(seqDb);

		itemFrequencies = itemFrequencies.entrySet().stream().filter(_e -> _e.getValue() >= this.minSupAbs)
				.collect(Collectors.toMap(_e -> _e.getKey(), _e -> _e.getValue()));

		return itemFrequencies;
	}

	public void filterPrefixesByMaxGap() {

		List<Sequence<Integer>> seqDb, prefixes, filteredPrefixes;
		Map<Sequence<Integer>, Integer> prefixMap = new HashMap<>();
		long maxGap = getMaxGap();
		
		if (maxGap <= 0) {
			return;
		}

		filteredPrefixes = new ArrayList<>();
		seqDb = this.getSeqDb();
		prefixes = this.getResPrefixes();

		for (Sequence<Integer> sequence : seqDb) {
			for (Sequence<Integer> prefix : prefixes) {
				if (prefix.size() > 1) {
					if (sequence.findSequence(prefix, maxGap)) {
						if (prefixMap.get(prefix) != null) {
							prefixMap.put(prefix, prefixMap.get(prefix) + 1);
						} else {
							prefixMap.put(prefix, 1);
						}
					}
				} else {
					prefixMap.put(prefix, 1);
				}
			}
		}
		
		for(Sequence<Integer> prefix : prefixMap.keySet()) {
			if (prefix.size() > 1) {
				//if (prefix.getFrequency() == prefixMap.get(prefix)) {
					prefix.setFrequency(prefixMap.get(prefix));
					filteredPrefixes.add(prefix);
				//}
			} else {
				filteredPrefixes.add(prefix);
			}
		}
		
		setResPrefixes(filteredPrefixes);
	}

}
