package com.sm.fpe.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.transaction.Transactional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.NotificationDao;
import com.sm.fpe.dao.UserDao;
import com.sm.fpe.model.Notification;
import com.sm.fpe.model.User;

@Service
public class NotificationService {

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private UserDao userDao;

	@Autowired
	private NotificationDao notificationDao;

	public List<Notification> getLast(int beforeId, int limit) {
		if (beforeId > 0) {
			return notificationDao.findByIdLessThanOrderByIdDesc(beforeId, new PageRequest(0, limit));
		} else {
			return notificationDao.findOrderByIdDesc(new PageRequest(0, limit));
		}
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<Notification> notifications) {

		DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		JSONObject obj, notificationJSON;
		JSONArray notificationsJSON;

		obj = new JSONObject();
		notificationsJSON = new JSONArray();

		for (Notification notification : notifications) {
			notificationJSON = new JSONObject();
			notificationJSON.put("id", Integer.toString(notification.getId()));
			notificationJSON.put("message", notification.getMessage());
			notificationJSON.put("timestamp", format.format(notification.getTimestamp()));
			notificationJSON.put("user", notification.getUser().getEmail());
			notificationJSON.put("read", notification.getRead().toString());
			notificationsJSON.add(notificationJSON);
		}

		obj.put("data", notificationsJSON);

		return obj.toJSONString();
	}

	@Transactional
	public int createAndSendNotification(String message) {

		Notification notification;

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetails = (UserDetails) auth.getPrincipal();
		User user = userDao.findByEmail(userDetails.getUsername());

		notification = new Notification();
		notification.setMessage(message);
		notification.setRead(false);
		notification.setTimestamp(new Timestamp(System.currentTimeMillis()));
		notification.setUser(user);

		notification = notificationDao.save(notification);

		this.template.convertAndSend("/topic/messages", notification.toJSON());
		
		return notification.getId();
	}
}
