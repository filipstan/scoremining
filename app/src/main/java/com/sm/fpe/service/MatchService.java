package com.sm.fpe.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.MatchDao;
import com.sm.fpe.model.Match;

@Service
public class MatchService {

	@Autowired
	private MatchDao matchDao;

	public List<Match> getMatches(String fromDate, String toDate) throws ParseException {

		Timestamp fromTt, toTt;
		Calendar now;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		if (fromDate != null && !fromDate.isEmpty()) {
			fromTt = new Timestamp(formatter.parse(fromDate).getTime());
		} else {
			now = Calendar.getInstance();
			now.add(Calendar.DATE, -1);
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			fromTt = new Timestamp(now.getTimeInMillis());
		}

		if (toDate != null && !toDate.isEmpty()) {
			toTt = new Timestamp(formatter.parse(toDate).getTime() - 1);
		} else {
			now = Calendar.getInstance();
			now.add(Calendar.DATE, -1);
			now.set(Calendar.HOUR_OF_DAY, 23);
			now.set(Calendar.MINUTE, 59);
			now.set(Calendar.SECOND, 59);
			now.set(Calendar.MILLISECOND, 999);
			toTt = new Timestamp(now.getTimeInMillis());
		}

		return matchDao.findByTimestampBetween(fromTt, toTt);
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<Match> matches) {

		JSONObject obj;
		JSONArray matchJSON, matchesJSON;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

		obj = new JSONObject();
		matchesJSON = new JSONArray();

		for (Match match : matches) {
			matchJSON = new JSONArray();
			matchJSON.add(Integer.toString(match.getId()));
			matchJSON.add(match.getHomeTeam().getName());
			matchJSON.add(match.getAwayTeam().getName());
			matchJSON.add(match.getHomeScore() + " - " + match.getAwayScore());
			matchJSON.add(match.getCompetition().getName());
			matchJSON.add(match.getCompetition().getCountry().getName());
			matchJSON.add(formatter.format(match.getTimestamp()));
			matchesJSON.add(matchJSON);
		}

		obj.put("data", matchesJSON);

		return obj.toJSONString();
	}
}
