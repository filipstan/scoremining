package com.sm.fpe.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.MatchDao;
import com.sm.fpe.dao.MatchPredictionDao;
import com.sm.fpe.dao.MatchUnplayedDao;
import com.sm.fpe.dao.TeamDao;
import com.sm.fpe.model.Match;
import com.sm.fpe.model.MatchPrediction;
import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.model.Team;

@Service
public class TeamService {

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private MatchUnplayedDao matchUnplayedDao;
	
	@Autowired
	private MatchPredictionDao matchPredictionDao;

	public List<Team> getAllTeams() {
		return teamDao.findAll();
	}

	public List<Team> getDuplicateTeams() {
		return teamDao.findAllDuplicatesByName();
	}

	public String mergeTeams(int keepTeamId, int removeTeamId) {

		Team keepTeam, removeTeam;
		List<Match> matches;
		List<MatchUnplayed> matchesUnplayed;
		List<MatchPrediction> matchPredictions;
		
		keepTeam = teamDao.findOne(keepTeamId);
		removeTeam = teamDao.findOne(removeTeamId);

		if (keepTeam != null && removeTeam != null) {

			for (Match _match : matchDao.findByHomeTeamOrAwayTeam(removeTeam, removeTeam)) {
				if (_match.getHomeTeam().equals(removeTeam)) {
					_match.setHomeTeam(keepTeam);
				} else {
					_match.setAwayTeam(keepTeam);
				}

				matchDao.save(_match);
			}

			for (MatchUnplayed _match : matchUnplayedDao.findByHomeTeamOrAwayTeam(removeTeam, removeTeam)) {
				if (_match.getHomeTeam().equals(removeTeam)) {
					_match.setHomeTeam(keepTeam);
				} else {
					_match.setAwayTeam(keepTeam);
				}

				matchUnplayedDao.save(_match);
			}

			teamDao.delete(removeTeam);

			while (matchDao.findDuplicates().size() > 0) {
				
				matches = matchDao.findDuplicates();
				
				matchDao.delete(matches);
			}

			while (matchUnplayedDao.findDuplicates().size() > 0) {
				
				matchesUnplayed = matchUnplayedDao.findDuplicates();
				
				for (MatchUnplayed _match : matchesUnplayed) {
					matchPredictions = matchPredictionDao.findByMatchUnplayed(_match);
					matchPredictionDao.delete(matchPredictions);
				}
				
				matchUnplayedDao.delete(matchesUnplayed);
			}

			return "Removed team " + removeTeam.getId() + " replaced with " + keepTeam.getId() + ".";
		}

		return "Invalid team ids.";
	}

	public String mergeAllTeams() {

		int keepTeamId = 0, removeTeamId = 0, nbrMerges = 0;
		List<Team> teams, duplicateTeams;
		List<String> countryNames = Arrays.asList("WORLD", "EUROPE", "SOUTHAMERICA", "ASIA", "NORTHCENTRALAMERICA",
				"AFRICA");
		StringBuilder ret = new StringBuilder();

		teams = this.getDuplicateTeams();

		for (Team team1 : teams) {

			duplicateTeams = new ArrayList<>();

			duplicateTeams.add(team1);

			for (Team team2 : teams) {
				if (team1.getId() != team2.getId() && team1.getName().equals(team2.getName())) {
					duplicateTeams.add(team2);
				}
			}

			if (duplicateTeams.size() == 2) {

				keepTeamId = 0;
				removeTeamId = 0;

				if (countryNames.contains(duplicateTeams.get(0).getCountry().getName())
						&& !countryNames.contains(duplicateTeams.get(1).getCountry().getName())) {
					keepTeamId = duplicateTeams.get(1).getId();
					removeTeamId = duplicateTeams.get(0).getId();
				} else if (countryNames.contains(duplicateTeams.get(1).getCountry().getName())
						&& !countryNames.contains(duplicateTeams.get(0).getCountry().getName())) {
					keepTeamId = duplicateTeams.get(0).getId();
					removeTeamId = duplicateTeams.get(1).getId();
				} else {
					if (duplicateTeams.get(0).getCountry().getName().equals("WORLD")
							&& countryNames.contains(duplicateTeams.get(1).getCountry().getName())) {
						keepTeamId = duplicateTeams.get(1).getId();
						removeTeamId = duplicateTeams.get(0).getId();
					}

					if (countryNames.contains(duplicateTeams.get(0).getCountry().getName())
							&& duplicateTeams.get(1).getCountry().getName().equals("WORLD")) {
						keepTeamId = duplicateTeams.get(0).getId();
						removeTeamId = duplicateTeams.get(1).getId();
					}
				}

				nbrMerges++;
				ret.append(this.mergeTeams(keepTeamId, removeTeamId));
			}
		}

		return nbrMerges + " teams were merged.";
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<Team> teams) {

		JSONObject obj;
		JSONArray teamJSON, teamsJSON;

		obj = new JSONObject();
		teamsJSON = new JSONArray();

		for (Team team : teams) {
			teamJSON = new JSONArray();
			teamJSON.add(Integer.toString(team.getId()));
			teamJSON.add(team.getName());
			teamJSON.add(team.getCountry().getName());
			teamJSON.add(team.getLevel() != null ? team.getLevel().toString() : "");
			teamsJSON.add(teamJSON);
		}

		obj.put("data", teamsJSON);

		return obj.toJSONString();
	}
}
