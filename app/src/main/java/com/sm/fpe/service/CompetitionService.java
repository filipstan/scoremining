package com.sm.fpe.service;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.CompetitionDao;
import com.sm.fpe.model.Competition;

@Service
public class CompetitionService {

	@Autowired
	private CompetitionDao competitionDao;

	public List<Competition> getAllCompetitions() {
		return competitionDao.findAllByOrderByCountry_NameAsc();
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<Competition> competitions) {

		JSONObject competitionJSON;
		JSONArray competitionsJSON;

		competitionJSON = new JSONObject();
		competitionsJSON = new JSONArray();

		for (Competition competition : competitions) {
			competitionJSON = new JSONObject();
			competitionJSON.put("id", competition.getId());
			competitionJSON.put("name", competition.getName());
			competitionJSON.put("country", competition.getCountry().getName());
			competitionsJSON.add(competitionJSON);
		}

		return competitionsJSON.toJSONString();
	}

}
