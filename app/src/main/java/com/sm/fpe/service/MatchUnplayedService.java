package com.sm.fpe.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.MatchUnplayedDao;
import com.sm.fpe.model.MatchUnplayed;

@Service
public class MatchUnplayedService {

	@Autowired
	private MatchUnplayedDao matchUnplayedDao;

	public List<MatchUnplayed> getUnplayedMatches(String fromDate, String toDate) throws ParseException {

		Timestamp fromTt, toTt;
		Calendar now;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		if (fromDate != null && !fromDate.isEmpty()) {
			fromTt = new Timestamp(formatter.parse(fromDate).getTime());
		} else {
			now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			fromTt = new Timestamp(now.getTimeInMillis());
		}

		if (toDate != null && !toDate.isEmpty()) {
			toTt = new Timestamp(formatter.parse(toDate).getTime() - 1);
		} else {
			now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 23);
			now.set(Calendar.MINUTE, 59);
			now.set(Calendar.SECOND, 59);
			now.set(Calendar.MILLISECOND, 999);
			toTt = new Timestamp(now.getTimeInMillis());
		}

		return matchUnplayedDao.findByTimestampBetween(fromTt, toTt);
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<MatchUnplayed> matchesUnplayed) {

		JSONObject obj;
		JSONArray matchUnplayedJSON, matchesUnplayedJSON;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

		obj = new JSONObject();
		matchesUnplayedJSON = new JSONArray();

		for (MatchUnplayed matchUnplayed : matchesUnplayed) {
			matchUnplayedJSON = new JSONArray();
			matchUnplayedJSON.add(Integer.toString(matchUnplayed.getId()));
			matchUnplayedJSON.add(matchUnplayed.getHomeTeam().getName());
			matchUnplayedJSON.add(matchUnplayed.getAwayTeam().getName());
			matchUnplayedJSON.add(matchUnplayed.getCompetition().getName());
			matchUnplayedJSON.add(matchUnplayed.getCompetition().getCountry().getName());
			matchUnplayedJSON.add(formatter.format(matchUnplayed.getTimestamp()));
			matchesUnplayedJSON.add(matchUnplayedJSON);
		}

		obj.put("data", matchesUnplayedJSON);

		return obj.toJSONString();
	}
}
