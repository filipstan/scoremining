package com.sm.fpe.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sm.fpe.dao.CompetitionDao;
import com.sm.fpe.dao.CountryDao;
import com.sm.fpe.dao.MatchDao;
import com.sm.fpe.dao.MatchUnplayedDao;
import com.sm.fpe.dao.TeamDao;
import com.sm.fpe.model.Competition;
import com.sm.fpe.model.Country;
import com.sm.fpe.model.Match;
import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.model.Team;
import com.sm.fpe.model.TeamLevel;

@Service
public class DataUtils {

	private List<String> generalCountryNames = Arrays.asList("EUROPE", "SOUTHAMERICA", "WORLD", "ASIA");

	@Autowired
	private CompetitionDao competitionDao;

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private MatchUnplayedDao matchUnplayedDao;

	@Value("${crawler.jsonfiles.path}")
	private String INPUT_FILES_PATH;

	@Value("${crawler.procesed.jsonfiles.path}")
	private String PROCESED_FILES_PATH;

	public void saveAllFilesToDb() {

		try (Stream<Path> paths = Files.walk(Paths.get(INPUT_FILES_PATH))) {

			paths.forEach(filePath -> {

				if (Files.isRegularFile(filePath)) {

					long startTime, stopTime, elapsedTime;
					int nbrMatchesSaved = 0, nbrUnplayedMatchesSaved = 0;

					startTime = System.currentTimeMillis();

					if (filePath.getFileName().toString().startsWith("m")) {
						nbrMatchesSaved = saveFileToDb(filePath.getFileName().toString(), false);
					} else {
						nbrUnplayedMatchesSaved = saveFileToDb(filePath.getFileName().toString(), true);
					}

					stopTime = System.currentTimeMillis();

					elapsedTime = stopTime - startTime;

					if (nbrMatchesSaved != 0) {
						System.out.println("File: " + filePath.getFileName() + " was saved to database with "
								+ nbrMatchesSaved + " matches [" + elapsedTime + " ms].");
					} else {
						System.out.println("File: " + filePath.getFileName() + " was saved to database with "
								+ nbrUnplayedMatchesSaved + " unplayed matches [" + elapsedTime + " ms].");
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int saveFileToDb(String filename, boolean matchUnplayedFlag) {

		InputStreamReader fr = null;
		JSONArray matches = null;
		int nbrMatchesSaved = 0;

		try {
			fr = new InputStreamReader(new FileInputStream(INPUT_FILES_PATH + filename), "UTF-8");
			JSONParser parser = new JSONParser();
			matches = (JSONArray) parser.parse(fr);
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		for (Object o : matches) {

			JSONObject match = (JSONObject) o;

			String countryName = (String) match.get("country");
			Country country = countryDao.saveIfNotExists(new Country(countryName));

			String tournament = (String) match.get("tournament");
			Competition competition = competitionDao.saveIfNotExists(new Competition(tournament, country));

			String timer = (String) match.get("timer");

			// skip this match if is not finished (FRO, Postponed, Abandoned,
			// After Pen., After ET)
			if (!matchUnplayedFlag && !timer.contains("Finished")) {
				continue;
			}

			String teamHomeName = (String) match.get("team_home");
			Team teamHome = teamDao.saveIfNotExists(new Team(teamHomeName, country));

			String teamAwayName = (String) match.get("team_away");
			Team teamAway = teamDao.saveIfNotExists(new Team(teamAwayName, country));

			String time = (String) match.get("time");
			DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Timestamp timestamp = null;

			try {
				timestamp = new Timestamp(format.parse(time).getTime());
			} catch (java.text.ParseException e) {
				e.printStackTrace();
			}

			if (!matchUnplayedFlag) {
				String score = (String) match.get("score");
				String[] scoreTokens = score.split("[^0-9]+");

				int scoreHome = Integer.parseInt(scoreTokens[0]);
				int scoreAway = Integer.parseInt(scoreTokens[1]);

				matchDao.saveIfNotExists(new Match(teamHome, teamAway, competition, scoreHome, scoreAway, timestamp));
			} else {
				matchUnplayedDao.saveIfNotExists(new MatchUnplayed(teamHome, teamAway, competition, timestamp));
			}
			nbrMatchesSaved++;
		}

		try {
			Files.move(Paths.get(INPUT_FILES_PATH + filename), Paths.get(PROCESED_FILES_PATH + filename),
					REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return nbrMatchesSaved;
	}

	@Transactional
	public void classifyTeams() {

		Map<Integer, Double> standings = new HashMap<>();
		List<Team> teams = teamDao.findAll();
		double maxPoints = 0;
		double points;
		int nbrTeamLevels = TeamLevel.values().length;
		TeamLevel teamLevel;

		for (Team team : teams) {

			points = 0;

			for (Match match : team.getHomeMatches()) {
				points += match.getHomePoints();
			}

			for (Match match : team.getAwayMatches()) {
				points += match.getAwayPoints();
			}

			if (points > maxPoints) {
				maxPoints = points;
			}

			standings.put(team.getId(), points);
		}

		// System.out.println(maxPoints);

		Team team;

		for (Integer teamId : standings.keySet()) {
			double chunkSize = maxPoints / nbrTeamLevels;
			teamLevel = TeamLevel.getTeamLevel((int) Math.ceil(standings.get(teamId) / chunkSize));
			team = teamDao.findOne(teamId);
			team.setLevel(teamLevel);
			teamDao.save(team);
		}
	}

	public void mergeTeams() {

		List<Team> teams = teamDao.findDuplicates();

		System.out.println(teams.size());
		// System.out.println(teams);

		teams.forEach(_team -> {

			List<Team> _teams;
			Team teamToDelete = null, teamToKeep = null;
			_teams = teamDao.findByName(_team.getName());

			if (_teams.size() == 2) {

				if (this.generalCountryNames.contains((Object) _teams.get(0).getCountry().getName())) {
					if (!this.generalCountryNames.contains((Object) _teams.get(1).getCountry().getName())) {
						teamToDelete = _teams.get(0);
						teamToKeep = _teams.get(1);
					}
				} else {
					if (this.generalCountryNames.contains((Object) _teams.get(1).getCountry().getName())) {
						teamToDelete = _teams.get(1);
						teamToKeep = _teams.get(0);
					}
				}

				if (teamToDelete != null && teamToKeep != null) {

					for (Match _match : matchDao.findByHomeTeamOrAwayTeam(teamToDelete, teamToDelete)) {
						if (_match.getHomeTeam().equals(teamToDelete)) {
							_match.setHomeTeam(teamToKeep);
						} else {
							_match.setAwayTeam(teamToKeep);
						}

						matchDao.save(_match);
					}

					System.out.println("Remove team " + teamToDelete + " replace with " + teamToKeep);
					teamDao.delete(teamToDelete);
				}
			}
		});

		System.out.println(matchDao.findDuplicates().size());
		System.out.println(matchDao.findDuplicates());

		while (matchDao.findDuplicates().size() > 0) {
			matchDao.delete(matchDao.findDuplicates());
		}
	}

	public MatchUnplayed convertMatchPlayedToUnplayed(int matchId) {

		Match match;
		MatchUnplayed matchUnplayed;

		match = matchDao.findOne(matchId);

		if (match == null) {
			throw new RuntimeException(
					"Cannot convert Match with id " + matchId + " to a MatchUnplayed beacause this Match not exists.");
		}

		matchUnplayed = new MatchUnplayed(match);

		matchUnplayed = matchUnplayedDao.saveIfNotExists(matchUnplayed);

		return matchUnplayed;
		// matchDao.delete(match);
	}

	public void convertMatchesToUnplayed(Timestamp start, Timestamp end) {

		matchDao.findByTimestampBetween(start, end).forEach(_match -> {
			this.convertMatchPlayedToUnplayed(_match.getId());
		});
	}

	public List<String> listMpFiles() {

		List<String> listFiles = new ArrayList<String>();

		try (Stream<Path> paths = Files.walk(Paths.get(INPUT_FILES_PATH))) {

			paths.forEach(filePath -> {

				if (Files.isRegularFile(filePath)) {

					if (filePath.getFileName().toString().startsWith("m")) {
						listFiles.add(filePath.getFileName().toString());
					}

				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listFiles;
	}

	public List<String> listMuFiles() {

		List<String> listFiles = new ArrayList<String>();

		try (Stream<Path> paths = Files.walk(Paths.get(INPUT_FILES_PATH))) {

			paths.forEach(filePath -> {

				if (Files.isRegularFile(filePath)) {

					if (filePath.getFileName().toString().startsWith("u")) {
						listFiles.add(filePath.getFileName().toString());
					}

				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}

		return listFiles;
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<String> list) {

		JSONObject objJSON;
		JSONArray arrayJSON;

		arrayJSON = new JSONArray();

		for (String item : list) {
			objJSON = new JSONObject();
			objJSON.put("name", item);
			arrayJSON.add(objJSON);
		}

		return arrayJSON.toJSONString();
	}

}
