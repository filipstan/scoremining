package com.sm.fpe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.UserDao;
import com.sm.fpe.model.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	public List<User> getAllUsers() {
		return userDao.findAll();
	}

}
