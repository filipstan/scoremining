package com.sm.fpe.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.Item;
import com.sm.fpe.ScoreType;
import com.sm.fpe.Sequence;
import com.sm.fpe.dao.MatchDao;
import com.sm.fpe.dao.MatchPredictionDao;
import com.sm.fpe.dao.MatchUnplayedDao;
import com.sm.fpe.dao.ProfileDao;
import com.sm.fpe.model.Match;
import com.sm.fpe.model.MatchPrediction;
import com.sm.fpe.model.MatchUnplayed;
import com.sm.fpe.model.Profile;
import com.sm.fpe.model.Team;

@Service
public class PredictionService {

	@Autowired
	private MatchDao matchDao;

	@Autowired
	private MatchUnplayedDao matchUnplayedDao;

	@Autowired
	private MatchPredictionDao matchPredictionDao;

	@Autowired
	private ProfileDao profileDao;

	@Autowired
	private SequenceMiningService smService;

	/*
	 * public List<MatchUnplayed> getPredictions() {
	 * 
	 * long totalAccuracy = 0; int count = 0; List<Match> matches; Match match;
	 * int accuracy; long startTime, stopTime, elapsedTime;
	 * List<MatchPrediction> matchPredictions; Map<Integer, Integer> homeScores;
	 * Map<Integer, Integer> awayScores; int maxTrust; List<MatchUnplayed>
	 * matchesUnplayed = matchUnplayedDao.findAll();
	 * 
	 * for (MatchUnplayed _matchUnplayed : matchesUnplayed) {
	 * 
	 * startTime = System.currentTimeMillis();
	 * 
	 * homeScores = new HashMap<>(); awayScores = new HashMap<>();
	 * 
	 * for (ScoreType scoreType : ScoreType.values()) {
	 * 
	 * matchPredictions =
	 * matchPredictionDao.findByMatchUnplayedAndScoreTypeOrderByTrustDesc(
	 * _matchUnplayed, scoreType);
	 * 
	 * maxTrust = 0;
	 * 
	 * for (MatchPrediction matchPrediction : matchPredictions) { // if
	 * (maxTrust < matchPrediction.getTrust()) { // maxTrust =
	 * matchPrediction.getTrust(); // }
	 * 
	 * // if (maxTrust == matchPrediction.getTrust()) { //
	 * System.out.println(matchPrediction.getScoreType() + " // score " +
	 * matchPrediction.getScore() // + " trust " + matchPrediction.getTrust());
	 * switch (scoreType) { case HomeScoreHomeTeamGiven: case
	 * HomeScoreAwayTeamTaken: case HomeAndAwayScoreHomeTeamGiven: case
	 * HomeAndAwayScoreAwayTeamTaken: case
	 * HomeScoreHomeTeamAgainstTeamLevelGiven: case
	 * HomeScoreAwayTeamAgainstTeamLevelTaken: case
	 * HomeAndAwayScoreHomeTeamAgainstTeamLevelGiven: case
	 * HomeAndAwayScoreAwayTeamAgainstTeamLevelTaken: if
	 * (homeScores.get(matchPrediction.getScore()) == null) {
	 * homeScores.put(matchPrediction.getScore(), matchPrediction.getTrust()); }
	 * else { homeScores.put(matchPrediction.getScore(),
	 * homeScores.get(matchPrediction.getScore()) + matchPrediction.getTrust());
	 * } break;
	 * 
	 * case AwayScoreAwayTeamGiven: case AwayScoreHomeTeamTaken: case
	 * HomeAndAwayScoreAwayTeamGiven: case HomeAndAwayScoreHomeTeamTaken: case
	 * AwayScoreAwayTeamAgainstTeamLevelGiven: case
	 * AwayScoreHomeTeamAgainstTeamLevelTaken: case
	 * HomeAndAwayScoreAwayTeanAgainstTeamLevelGiven: case
	 * HomeAndAwayScoreHomeTeamAgainstTeamLevelTaken: if
	 * (awayScores.get(matchPrediction.getScore()) == null) {
	 * awayScores.put(matchPrediction.getScore(), matchPrediction.getTrust()); }
	 * else { awayScores.put(matchPrediction.getScore(),
	 * awayScores.get(matchPrediction.getScore()) + matchPrediction.getTrust());
	 * } break; } // } } }
	 * 
	 * _matchUnplayed.setHomeScorePredictions(homeScores);
	 * _matchUnplayed.setAwayScorePredictions(awayScores);
	 * 
	 * int homeScore = -1, awayScore = -1, frequency, maxFrequency;
	 * 
	 * maxFrequency = 0;
	 * 
	 * for (int score : homeScores.keySet()) { frequency =
	 * homeScores.get(score); if (maxFrequency < frequency) { maxFrequency =
	 * frequency; homeScore = score; } }
	 * 
	 * maxFrequency = 0;
	 * 
	 * for (int score : awayScores.keySet()) { frequency =
	 * awayScores.get(score); if (maxFrequency < frequency) { maxFrequency =
	 * frequency; awayScore = score; } }
	 * 
	 * stopTime = System.currentTimeMillis(); elapsedTime = stopTime -
	 * startTime;
	 * 
	 * matches = matchDao.findByCompetitionAndHomeTeamAndAwayTeamAndTimestamp(
	 * _matchUnplayed.getCompetition(), _matchUnplayed.getHomeTeam(),
	 * _matchUnplayed.getAwayTeam(), _matchUnplayed.getTimestamp());
	 * 
	 * if (matches.size() > 1) { throw new RuntimeException(
	 * "Found multiple matches with same competition, home team, away team and timestamp."
	 * ); }
	 * 
	 * if (matches.size() == 0) {
	 * System.out.println(_matchUnplayed.getHomeTeam().getName() + " - " +
	 * _matchUnplayed.getAwayTeam().getName() + homeScores + awayScores +
	 * ". Prediction is " + homeScore + " - " + awayScore + ". [" + elapsedTime
	 * + " ms]"); continue; }
	 * 
	 * match = matches.get(0);
	 * 
	 * accuracy =
	 * MatchPrediction.compareMatchScoreWithPrediction(match.getHomeScore(),
	 * match.getAwayScore(), homeScore, awayScore); totalAccuracy += accuracy;
	 * 
	 * if (homeScore != -1) { count++; System.out.println(
	 * _matchUnplayed.getHomeTeam().getName() + " - " +
	 * _matchUnplayed.getAwayTeam().getName() + " " + match.getHomeScore() +
	 * " - " + match.getAwayScore() + ". Prediction is " + homeScore + " - " +
	 * awayScore + ". Accuracy is " + accuracy + " %. [" + elapsedTime +
	 * " ms]"); } }
	 * 
	 * System.out.println("Total accuracy is " + (double) totalAccuracy / count
	 * + " %.");
	 * 
	 * return matchesUnplayed; }
	 */

	public List<MatchUnplayed> findMatchesUnplayed(Timestamp fromTt, Timestamp toTt) {
		List<MatchUnplayed> matchesUnplayed;
		List<MatchPrediction> matchesPredictions;

		matchesPredictions = matchPredictionDao.findMatchesUnplayedByTimestampBetween(fromTt, toTt);

		matchesUnplayed = new ArrayList<MatchUnplayed>();

		for (MatchPrediction matchPrediction : matchesPredictions) {
			matchesUnplayed.add(matchPrediction.getMatchUnplayed());
		}

		return matchesUnplayed;
	}

	public List<Profile> findProfilesByMatchUnplayed(MatchUnplayed matchUnplayed) {
		List<Profile> profiles;
		List<MatchPrediction> matchesPredictions;

		matchesPredictions = matchPredictionDao.findProfilesByMatchUnplayed(matchUnplayed);

		profiles = new ArrayList<Profile>();

		for (MatchPrediction matchPrediction : matchesPredictions) {
			profiles.add(matchPrediction.getProfile());
		}

		return profiles;
	}

	@SuppressWarnings("unchecked")
	public String getMatchesPredictionsGroupedJSON(String fromDate, String toDate, String onlyTrusty)
			throws ParseException {

		List<MatchPrediction> matchPredictions;
		List<MatchUnplayed> matchesUnplayed;
		List<Profile> profiles;
		JSONArray jsonArray, jsonItem = null;
		JSONObject jsonObject;
		boolean _onlyTrusty;

		_onlyTrusty = Boolean.parseBoolean(onlyTrusty);

		jsonObject = new JSONObject();
		jsonArray = new JSONArray();

		Timestamp fromTt, toTt;
		Calendar now;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		if (fromDate != null && !fromDate.isEmpty()) {
			fromTt = new Timestamp(formatter.parse(fromDate).getTime());
		} else {
			now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			fromTt = new Timestamp(now.getTimeInMillis());
		}

		if (toDate != null && !toDate.isEmpty()) {
			toTt = new Timestamp(formatter.parse(toDate).getTime() - 1);
		} else {
			now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 23);
			now.set(Calendar.MINUTE, 59);
			now.set(Calendar.SECOND, 59);
			now.set(Calendar.MILLISECOND, 999);
			toTt = new Timestamp(now.getTimeInMillis());
		}

		matchesUnplayed = findMatchesUnplayed(fromTt, toTt);

		for (MatchUnplayed _matchUnplayed : matchesUnplayed) {

			profiles = findProfilesByMatchUnplayed(_matchUnplayed);

			for (Profile _profile : profiles) {

				matchPredictions = matchPredictionDao.findByMatchUnplayedAndProfile(_matchUnplayed, _profile);

				switch (_profile.getStrategyType()) {
				case BiggestTrustSum:
					jsonItem = getPredictionsByStrategyBiggestTrustSum(matchPredictions, _onlyTrusty);
					break;
				case MostFrequentScore:
					jsonItem = getPredictionsByStrategyMostFrequentScore(matchPredictions, _onlyTrusty);
					break;
				}

				if (jsonItem != null) {
					jsonArray.add(jsonItem);
				}
			}
		}

		jsonObject.put("data", jsonArray);

		return jsonObject.toJSONString();
	}

	// matchPredictions are for only one match and one profile
	private JSONArray getPredictionsByStrategyMostFrequentScore(List<MatchPrediction> matchPredictions,
			boolean onlyTrusty) {

		if (matchPredictions == null || matchPredictions.isEmpty()) {
			throw new RuntimeException("matchPredictions list is null or empty");
		}

		MatchUnplayed matchUnplayed = null;
		Profile profile;
		Map<Integer, Integer> homeScores, awayScores;
		int homeScore, awayScore, homeScore2, awayScore2, bFactor = 0;
		String scorePredicted = "", scorePredicted2 = "", scorePredictedSource = "";

		homeScores = new HashMap<>();
		awayScores = new HashMap<>();

		for (MatchPrediction matchPrediction : matchPredictions) {

			if (matchPrediction.getScoreType().isForHomeTeam()) {
				if (homeScores.get(matchPrediction.getScore()) == null) {
					homeScores.put(matchPrediction.getScore(), 1);
				} else {
					homeScores.put(matchPrediction.getScore(), homeScores.get(matchPrediction.getScore()) + 1);
				}
			} else {
				if (awayScores.get(matchPrediction.getScore()) == null) {
					awayScores.put(matchPrediction.getScore(), 1);
				} else {
					awayScores.put(matchPrediction.getScore(), awayScores.get(matchPrediction.getScore()) + 1);
				}
			}
		}

		matchUnplayed = matchPredictions.get(0).getMatchUnplayed();

		profile = matchPredictions.get(0).getProfile();

		try {
			homeScore = getIndexForMaxValue(homeScores);
			scorePredicted += homeScore;
			scorePredictedSource = homeScores.toString();

			homeScore2 = getIndexForMaxValue2(homeScores);
			scorePredicted2 += homeScore2;
			bFactor += homeScores.get(homeScore);
		} catch (RuntimeException e) {
		}

		scorePredicted += " - ";
		scorePredicted2 += " - ";
		scorePredictedSource += " - ";

		try {
			awayScore = getIndexForMaxValue(awayScores);
			scorePredicted += awayScore;
			scorePredictedSource += awayScores.toString();

			awayScore2 = getIndexForMaxValue2(awayScores);
			scorePredicted2 += awayScore2;
			bFactor += awayScores.get(awayScore);
		} catch (RuntimeException e) {
		}

		return createMatchPredictionJSON(matchUnplayed, profile, scorePredicted, scorePredicted2,
				String.valueOf(bFactor), scorePredictedSource, onlyTrusty);
	}

	// matchPredictions are for only one match and one profile
	private JSONArray getPredictionsByStrategyBiggestTrustSum(List<MatchPrediction> matchPredictions,
			boolean onlyTrusty) {

		if (matchPredictions == null || matchPredictions.isEmpty()) {
			throw new RuntimeException("matchPredictions list is null or empty");
		}

		MatchUnplayed matchUnplayed = null;
		Profile profile;
		Map<Integer, Integer> homeScores, awayScores;
		int homeScore, awayScore, homeScore2, awayScore2, bFactor = 0;
		String scorePredicted = "", scorePredicted2 = "", scorePredictedSource = "";

		homeScores = new HashMap<>();
		awayScores = new HashMap<>();

		for (MatchPrediction matchPrediction : matchPredictions) {

			if (matchPrediction.getScoreType().isForHomeTeam()) {
				if (homeScores.get(matchPrediction.getScore()) == null) {
					homeScores.put(matchPrediction.getScore(), matchPrediction.getTrust());
				} else {
					homeScores.put(matchPrediction.getScore(),
							homeScores.get(matchPrediction.getScore()) + matchPrediction.getTrust());
				}
			} else {
				if (awayScores.get(matchPrediction.getScore()) == null) {
					awayScores.put(matchPrediction.getScore(), matchPrediction.getTrust());
				} else {
					awayScores.put(matchPrediction.getScore(),
							awayScores.get(matchPrediction.getScore()) + matchPrediction.getTrust());
				}
			}
		}

		matchUnplayed = matchPredictions.get(0).getMatchUnplayed();

		profile = matchPredictions.get(0).getProfile();

		try {
			homeScore = getIndexForMaxValue(homeScores);
			scorePredicted += homeScore;
			scorePredictedSource = homeScores.toString();

			homeScore2 = getIndexForMaxValue2(homeScores);
			scorePredicted2 += homeScore2;
			bFactor += homeScores.get(homeScore);
		} catch (RuntimeException e) {
		}

		scorePredicted += " - ";
		scorePredicted2 += " - ";
		scorePredictedSource += " - ";

		try {
			awayScore = getIndexForMaxValue(awayScores);
			scorePredicted += awayScore;
			scorePredictedSource += awayScores.toString();

			awayScore2 = getIndexForMaxValue2(awayScores);
			scorePredicted2 += awayScore2;
			bFactor += awayScores.get(awayScore);
		} catch (RuntimeException e) {
		}

		return createMatchPredictionJSON(matchUnplayed, profile, scorePredicted, scorePredicted2,
				String.valueOf(bFactor), scorePredictedSource, onlyTrusty);
	}

	@SuppressWarnings("unchecked")
	private JSONArray createMatchPredictionJSON(MatchUnplayed matchUnplayed, Profile profile, String scorePredicted,
			String scorePredicted2, String bFactor, String scorePredictedSource, boolean onlyTrusty) {

		JSONArray matchPredictionJSON;
		StringBuilder sb;
		Match match;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		boolean trustyHomeScore, trustyAwayScore;
		String isScoreTrusty, scorePredictedFormatted = "", scorePredictedConfidence = "";
		StringTokenizer st;
		int scoreHome = -1, scoreAway = -1, scoreHome2 = -1, scoreAway2 = -1;

		isScoreTrusty = isScoreTrusty2(scorePredictedSource);
		st = new StringTokenizer(isScoreTrusty, "-");
		trustyHomeScore = Boolean.parseBoolean(st.nextToken());
		trustyAwayScore = Boolean.parseBoolean(st.nextToken());

		st = new StringTokenizer(scorePredicted, " -");
		if (st.hasMoreTokens()) {
			scoreHome = Integer.parseInt(st.nextToken());
		}
		if (st.hasMoreTokens()) {
			scoreAway = Integer.parseInt(st.nextToken());
		}
		if (trustyHomeScore && trustyAwayScore) {
			if (Math.abs(scoreHome - scoreAway) > 1) {
				scorePredictedFormatted = "<strong class='text-success'>" + scoreHome + " - " + scoreAway + "</strong>";
			} else {
				scorePredictedFormatted = "<strong class='text-info'>" + scoreHome + " - " + scoreAway + "</strong>";
			}
		} else {

			if (onlyTrusty) {
				return null;
			}

			scorePredictedFormatted += (trustyHomeScore && scoreHome > -1 ? "<strong>" + scoreHome + "</strong>"
					: scoreHome);
			scorePredictedFormatted += " - ";
			scorePredictedFormatted += (trustyAwayScore && scoreAway > -1 ? "<strong>" + scoreAway + "</strong>"
					: scoreAway);
		}

		st = new StringTokenizer(scorePredicted2, " -");
		if (st.hasMoreTokens()) {
			scoreHome2 = Integer.parseInt(st.nextToken());
		}
		if (st.hasMoreTokens()) {
			scoreAway2 = Integer.parseInt(st.nextToken());
		}

		scorePredictedConfidence = getScorePredictedConfidence(scoreHome, scoreAway, scoreHome2, scoreAway2);

		sb = new StringBuilder();

		sb.append(matchUnplayed.getHomeTeam().getName());
		sb.append(" - ");
		sb.append(matchUnplayed.getAwayTeam().getName());

		matchPredictionJSON = new JSONArray();

		match = getMatchByMatchUnplayed(matchUnplayed);

		matchPredictionJSON.add(sb.toString());
		if (match != null) {
			matchPredictionJSON.add(match.getHomeScore() + " - " + match.getAwayScore());
		} else {
			matchPredictionJSON.add("N/A");
		}
		matchPredictionJSON.add(scorePredictedFormatted);
		matchPredictionJSON.add(scorePredicted2);
		matchPredictionJSON.add(scorePredictedConfidence);
		matchPredictionJSON.add(bFactor);
		matchPredictionJSON.add(matchUnplayed.getCompetition().getName());
		matchPredictionJSON.add(matchUnplayed.getCompetition().getCountry().getName());
		matchPredictionJSON.add(formatter.format(matchUnplayed.getTimestamp()));
		matchPredictionJSON.add(profile.getName());
		matchPredictionJSON.add(scorePredictedSource);

		return matchPredictionJSON;
	}

	private String getScorePredictedConfidence(int scoreHome, int scoreAway, int scoreHome2, int scoreAway2) {

		int scorePredictedConfidence = 0, a1, a2, a3, a4;

		a1 = scoreHome - scoreAway;
		a2 = scoreHome - scoreAway2;
		a3 = scoreHome2 - scoreAway;
		a4 = scoreHome2 - scoreAway2;

		a1 = (a1 > 0 ? 1 : (a1 == 0 ? 0 : 2));
		a2 = (a2 > 0 ? 1 : (a2 == 0 ? 0 : 2));
		a3 = (a3 > 0 ? 1 : (a3 == 0 ? 0 : 2));
		a4 = (a4 > 0 ? 1 : (a4 == 0 ? 0 : 2));

		scorePredictedConfidence += 4;

		if (a1 == a2) {
			scorePredictedConfidence += 1;
		}

		if (a1 == a3) {
			scorePredictedConfidence += 1;
		}

		if (a1 == a4) {
			scorePredictedConfidence += 4;
		}

		return "" + (scorePredictedConfidence * 10) + "%";
	}

	private String isScoreTrusty(String scorePredictedSource) {

		Map<Integer, Integer> homeScores, awayScores;
		String hScore, aScore;
		StringTokenizer stringTok;
		int value, diff, maxHomeTrust = -1, maxAwayTrust = -1, trustDiff;
		final int TRUST_EPS = 7, TRUST_LIMIT = 80, TRUST_MIN = 150;
		boolean trustyHomeScore = true, trustyAwayScore = true;

		homeScores = new HashMap<>();
		awayScores = new HashMap<>();
		stringTok = new StringTokenizer(scorePredictedSource, "-");
		hScore = stringTok.nextToken();
		aScore = stringTok.nextToken();

		stringTok = new StringTokenizer(hScore, "{}, =");
		while (stringTok.hasMoreTokens()) {
			homeScores.put(Integer.parseInt(stringTok.nextToken()), Integer.parseInt(stringTok.nextToken()));
		}

		stringTok = new StringTokenizer(aScore, "{}, =");
		while (stringTok.hasMoreTokens()) {
			awayScores.put(Integer.parseInt(stringTok.nextToken()), Integer.parseInt(stringTok.nextToken()));
		}

		trustyHomeScore = false;
		for (Integer key : homeScores.keySet()) {

			value = homeScores.get(key);

			if (value > maxHomeTrust) {
				maxHomeTrust = value;
			}

			if (value >= TRUST_MIN + TRUST_EPS) {
				trustyHomeScore = true;
			}
		}

		trustyAwayScore = false;
		for (Integer key : awayScores.keySet()) {

			value = awayScores.get(key);

			if (value > maxAwayTrust) {
				maxAwayTrust = value;
			}

			if (value >= TRUST_MIN + TRUST_EPS) {
				trustyAwayScore = true;
			}
		}

		for (Integer key1 : homeScores.keySet()) {
			for (Integer key2 : homeScores.keySet()) {
				if (trustyHomeScore && !key1.equals(key2) && homeScores.get(key1) > TRUST_LIMIT
						&& homeScores.get(key2) > TRUST_LIMIT) {
					diff = homeScores.get(key1) - homeScores.get(key2);
					diff = Math.abs(diff);
					if (diff <= TRUST_EPS) {
						trustyHomeScore = false;
					}
				}
			}
		}

		for (Integer key1 : awayScores.keySet()) {
			for (Integer key2 : awayScores.keySet()) {
				if (trustyAwayScore && !key1.equals(key2) && awayScores.get(key1) > TRUST_LIMIT
						&& awayScores.get(key2) > TRUST_LIMIT) {
					diff = awayScores.get(key1) - awayScores.get(key2);
					diff = Math.abs(diff);
					if (diff <= TRUST_EPS) {
						trustyAwayScore = false;
					}
				}
			}
		}

		return (trustyHomeScore ? "true" : "false") + "-" + (trustyAwayScore ? "true" : "false");
	}

	private String isScoreTrusty2(String scorePredictedSource) {

		Map<Integer, Integer> homeScores, awayScores;
		String hScore, aScore;
		StringTokenizer stringTok;
		int value, maxHomeTrust = -1, maxAwayTrust = -1, trustDiff;
		boolean trustyHomeScore = false, trustyAwayScore = false;

		homeScores = new HashMap<>();
		awayScores = new HashMap<>();
		stringTok = new StringTokenizer(scorePredictedSource, "-");
		hScore = stringTok.nextToken();
		aScore = stringTok.nextToken();

		stringTok = new StringTokenizer(hScore, "{}, =");
		while (stringTok.hasMoreTokens()) {
			homeScores.put(Integer.parseInt(stringTok.nextToken()), Integer.parseInt(stringTok.nextToken()));
		}

		stringTok = new StringTokenizer(aScore, "{}, =");
		while (stringTok.hasMoreTokens()) {
			awayScores.put(Integer.parseInt(stringTok.nextToken()), Integer.parseInt(stringTok.nextToken()));
		}

		for (Integer key : homeScores.keySet()) {

			value = homeScores.get(key);

			if (value > maxHomeTrust) {
				maxHomeTrust = value;
			}
		}

		for (Integer key : awayScores.keySet()) {

			value = awayScores.get(key);

			if (value > maxAwayTrust) {
				maxAwayTrust = value;
			}
		}

		if (homeScores.get(0) != null) {

			trustDiff = (int) Math.ceil(maxHomeTrust / 10);

			if (maxHomeTrust != homeScores.get(0)) {

				if (maxHomeTrust - homeScores.get(0) > trustDiff) {
					trustyHomeScore = true;
				}

			} else {

				trustyHomeScore = true;

				for (Integer key : homeScores.keySet()) {
					if (key != 0) {
						if (maxHomeTrust - homeScores.get(key) <= trustDiff) {
							trustyHomeScore = false;
						}
					}
				}
			}
		} else {
			trustyHomeScore = true;
		}

		if (awayScores.get(0) != null) {

			trustDiff = (int) Math.ceil(maxAwayTrust / 10);

			if (maxAwayTrust != awayScores.get(0)) {

				if (maxAwayTrust - awayScores.get(0) > trustDiff) {
					trustyAwayScore = true;
				}

			} else {

				trustyAwayScore = true;

				for (Integer key : awayScores.keySet()) {
					if (key != 0) {
						if (maxAwayTrust - awayScores.get(key) <= trustDiff) {
							trustyAwayScore = false;
						}
					}
				}
			}
		} else {
			trustyAwayScore = true;
		}

		return (trustyHomeScore ? "true" : "false") + "-" + (trustyAwayScore ? "true" : "false");
	}

	private int getIndexForMaxValue(Map<Integer, Integer> map) {

		int max = 0, maxKey = -1;

		for (int i : map.keySet()) {
			if (max < map.get(i)) {
				max = map.get(i);
				maxKey = i;
			}
		}

		if (maxKey == -1) {
			throw new RuntimeException("Not found max");
		}

		return maxKey;
	}

	private int getIndexForMaxValue2(Map<Integer, Integer> map) {

		int max = 0, max2 = 0, maxKey = -1, maxKey2 = -1;

		for (int i : map.keySet()) {
			if (max < map.get(i)) {
				max2 = max;
				maxKey2 = maxKey;
				max = map.get(i);
				maxKey = i;
			} else if (max2 < map.get(i)) {
				max2 = map.get(i);
				maxKey2 = i;
			}
		}

		if (maxKey2 == -1) {
			throw new RuntimeException("Not found max2");
		}

		return maxKey2;
	}

	public String computePrediction(int matchUnplayedId, int profileId) {

		MatchUnplayed matchUnplayed;
		Profile profile;

		matchUnplayed = matchUnplayedDao.findOne(matchUnplayedId);

		if (matchUnplayed == null) {
			throw new RuntimeException("MatchUnplayed with id " + matchUnplayedId + " not exists.");
		}

		profile = profileDao.findOne(profileId);

		if (profile == null) {
			throw new RuntimeException("Profile with id " + profileId + " not exists.");
		}

		matchPredictionDao.delete(matchPredictionDao.findByMatchUnplayedAndProfile(matchUnplayed, profile));

		this.computePrediction(matchUnplayed, profile);

		return "Predictions for " + matchUnplayed.getHomeTeam().getName() + " - "
				+ matchUnplayed.getAwayTeam().getName() + " with " + profile.getName() + " are ready.";
	}

	private void computePrediction(MatchUnplayed matchUnplayed, Profile profile) {

		List<ScoreType> scoreTypes = profile.getScoreTypesList();

		for (ScoreType scoreType : scoreTypes) {
			System.gc();
			this.computePrediction(matchUnplayed, profile, scoreType);
		}
	}

	private void computePrediction(MatchUnplayed matchUnplayed, Profile profile, ScoreType scoreType) {

		List<Sequence<Integer>> sequences;
		List<Sequence<Integer>> prefixes;
		List<Sequence<Integer>> goodPrefixes = new ArrayList<>();
		Sequence<Integer> refSequence;
		long startTime, stopTime, elapsedTime;

		smService.setMinSup((double) profile.getMinSup() / 100);

		smService.setMaxGap(profile.getMaxGap());

		startTime = System.currentTimeMillis();

		sequences = smService.getSequences(matchUnplayed, scoreType, profile);

		stopTime = System.currentTimeMillis();

		elapsedTime = stopTime - startTime;

		System.out.println(
				"Get " + sequences.size() + " sequences for score type " + scoreType + " [" + elapsedTime + " ms].");

		refSequence = smService.getSequence(matchUnplayed, scoreType);

		smService.runPrefixSpan(sequences);
		smService.filterPrefixesByMaxGap();
		prefixes = smService.getResPrefixes();

		startTime = System.currentTimeMillis();

		smService.getFrequentItems().forEach(_item -> {

			Sequence<Integer> _sequence;
			Sequence<Integer> sufixSequence;
			int index;

			_sequence = refSequence.copySequence();
			_sequence.add(new Item<>(_item.getValue()), matchUnplayed.getTimestamp());

			for (int i = _sequence.getList().size() - 1; i >= 0; i--) {
				// System.out.println(i + " " + _sequence.getList().size());
				sufixSequence = new Sequence<>(_sequence.getList().subList(i, _sequence.getList().size()));
				index = prefixes.indexOf(sufixSequence);

				if (index != -1) {
					goodPrefixes.add(prefixes.get(index));
				} else {
					break;
				}
			}
		});

		// System.out.println(scoreType);
		// System.out.println(sequences.size());
		// System.out.println(refSequence);
		// smService.printPrefixes(prefixes);
		// smService.printPrefixes(goodPrefixes);

		goodPrefixes.forEach(_sequence -> {

			int score, trust;
			MatchPrediction matchPrediction;

			score = _sequence.getLastItemValue();
			trust = this.getTrust(_sequence, scoreType, sequences.size(), profile);

			matchPrediction = new MatchPrediction(matchUnplayed, score, scoreType, trust, profile);

			// System.out.println(matchPrediction);

			matchPrediction = matchPredictionDao.saveIfNotExists(matchPrediction);
		});

		stopTime = System.currentTimeMillis();

		elapsedTime = stopTime - startTime;

		System.out.println("Found " + prefixes.size() + " prefixes." + " Found " + goodPrefixes.size()
				+ " good prefixes." + " Create and save predictions in database " + " [" + elapsedTime + " ms].");
	}

	private int getTrust(Sequence<Integer> prefix, ScoreType scoreType, int nbrSequences, Profile profile) {

		int prefixSize, frequency;
		String trustFormula;

		prefixSize = prefix.getList().size();
		frequency = prefix.getFrequency();
		trustFormula = profile.getTrustFormula();

		// System.out.println(trustFormula);

		trustFormula = trustFormula.replaceAll("prefixFrequency",
				Integer.toString((int) (((double) frequency * 100 / nbrSequences)) / 2));
		trustFormula = trustFormula.replaceAll("scoreType", Integer.toString(scoreType.getTrust()));
		trustFormula = trustFormula.replaceAll("prefixLength", Integer.toString(prefixSize));

		// System.out.println(trustFormula);

		return (int) eval(trustFormula);
	}

	public List<MatchPrediction> getMatchesPredictions(String fromDate, String toDate) throws ParseException {

		Timestamp fromTt, toTt;
		Calendar now;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		if (fromDate != null && !fromDate.isEmpty()) {
			fromTt = new Timestamp(formatter.parse(fromDate).getTime());
		} else {
			now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			fromTt = new Timestamp(now.getTimeInMillis());
		}

		if (toDate != null && !toDate.isEmpty()) {
			toTt = new Timestamp(formatter.parse(toDate).getTime() - 1);
		} else {
			now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 23);
			now.set(Calendar.MINUTE, 59);
			now.set(Calendar.SECOND, 59);
			now.set(Calendar.MILLISECOND, 999);
			toTt = new Timestamp(now.getTimeInMillis());
		}

		return matchPredictionDao.findByTimestampBetween(fromTt, toTt);
	}
	
	public String deleteMatchesPredictions(String fromDate, String toDate) throws ParseException {

		Timestamp fromTt, toTt;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		if (fromDate != null && !fromDate.isEmpty()) {
			fromTt = new Timestamp(formatter.parse(fromDate).getTime());
		} else {
			return "";
		}

		if (toDate != null && !toDate.isEmpty()) {
			toTt = new Timestamp(formatter.parse(toDate).getTime() - 1);
		} else {
			return "";
		}

		matchPredictionDao.delete(matchPredictionDao.findByTimestampBetween(fromTt, toTt));
		
		return "Predictions between " + fromTt + " and " + toTt + " were deleted";
	}
	
	@SuppressWarnings("unchecked")
	public String toJSON(List<MatchPrediction> matches) {

		Match match;
		JSONObject obj;
		JSONArray matchJSON, matchesJSON;
		StringBuilder sb;
		Team team;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

		obj = new JSONObject();
		matchesJSON = new JSONArray();

		for (MatchPrediction mp : matches) {

			match = getMatchByMatchUnplayed(mp.getMatchUnplayed());
			matchJSON = new JSONArray();
			team = getTeamByScoreType(mp.getMatchUnplayed(), mp.getScoreType());

			sb = new StringBuilder();
			if (mp.getMatchUnplayed().getHomeTeam().getId() == team.getId()) {
				sb.append("<b>" + mp.getMatchUnplayed().getHomeTeam().getName() + "</b>");
				sb.append(" - ");
				sb.append(mp.getMatchUnplayed().getAwayTeam().getName());
			} else {
				sb.append(mp.getMatchUnplayed().getHomeTeam().getName());
				sb.append(" - ");
				sb.append("<b>" + mp.getMatchUnplayed().getAwayTeam().getName() + "</b>");
			}
			sb.append(" ");
			if (match != null) {
				sb.append(match.getHomeScore() + " - " + match.getAwayScore());
			}
			sb.append(" (");
			sb.append(mp.getMatchUnplayed().getCompetition().getName());
			sb.append(" - ");
			sb.append(mp.getMatchUnplayed().getCompetition().getCountry().getName());
			sb.append(") ");
			sb.append(formatter.format(mp.getMatchUnplayed().getTimestamp()));
			matchJSON.add(sb.toString());
			matchJSON.add(Integer.toString(mp.getScore()));
			matchJSON.add(mp.getScoreType().toString());
			matchJSON.add(Integer.toString(mp.getTrust()));
			matchJSON.add(mp.getProfile().getName());
			matchesJSON.add(matchJSON);
		}

		obj.put("data", matchesJSON);

		return obj.toJSONString();
	}

	public Match getMatchByMatchUnplayed(MatchUnplayed mu) {

		List<Match> matches;

		matches = matchDao.findByCompetitionAndHomeTeamAndAwayTeamAndTimestamp(mu.getCompetition(), mu.getHomeTeam(),
				mu.getAwayTeam(), mu.getTimestamp());

		if (matches.size() > 1) {
			throw new RuntimeException(
					"Found multiple matches with same competition, home team, away team and timestamp.");
		}

		if (matches.size() == 0) {
			return null;
		}

		return matches.get(0);
	}

	public Team getTeamByScoreType(MatchUnplayed matchUnplayed, ScoreType scoreType) {

		Team team = null;

		switch (scoreType) {
		case HomeScoreHomeTeamGiven:
		case AwayScoreHomeTeamTaken:
		case HomeAndAwayScoreHomeTeamGiven:
		case HomeAndAwayScoreHomeTeamTaken:
		case HomeScoreHomeTeamAgainstTeamLevelGiven:
		case AwayScoreHomeTeamAgainstTeamLevelTaken:
		case HomeAndAwayScoreHomeTeamAgainstTeamLevelGiven:
		case HomeAndAwayScoreHomeTeamAgainstTeamLevelTaken:
			team = matchUnplayed.getHomeTeam();
			break;
		case AwayScoreAwayTeamGiven:
		case HomeScoreAwayTeamTaken:
		case HomeAndAwayScoreAwayTeamGiven:
		case HomeAndAwayScoreAwayTeamTaken:
		case AwayScoreAwayTeamAgainstTeamLevelGiven:
		case HomeScoreAwayTeamAgainstTeamLevelTaken:
		case HomeAndAwayScoreAwayTeanAgainstTeamLevelGiven:
		case HomeAndAwayScoreAwayTeamAgainstTeamLevelTaken:
			team = matchUnplayed.getAwayTeam();
			break;
		}

		return team;
	}

	public static double eval(final String str) {
		return new Object() {
			int pos = -1, ch;

			void nextChar() {
				ch = (++pos < str.length()) ? str.charAt(pos) : -1;
			}

			boolean eat(int charToEat) {
				while (ch == ' ')
					nextChar();
				if (ch == charToEat) {
					nextChar();
					return true;
				}
				return false;
			}

			double parse() {
				nextChar();
				double x = parseExpression();
				if (pos < str.length())
					throw new RuntimeException("Unexpected: " + (char) ch);
				return x;
			}

			// Grammar:
			// expression = term | expression `+` term | expression `-` term
			// term = factor | term `*` factor | term `/` factor
			// factor = `+` factor | `-` factor | `(` expression `)`
			// | number | functionName factor | factor `^` factor

			double parseExpression() {
				double x = parseTerm();
				for (;;) {
					if (eat('+'))
						x += parseTerm(); // addition
					else if (eat('-'))
						x -= parseTerm(); // subtraction
					else
						return x;
				}
			}

			double parseTerm() {
				double x = parseFactor();
				for (;;) {
					if (eat('*'))
						x *= parseFactor(); // multiplication
					else if (eat('/'))
						x /= parseFactor(); // division
					else
						return x;
				}
			}

			double parseFactor() {
				if (eat('+'))
					return parseFactor(); // unary plus
				if (eat('-'))
					return -parseFactor(); // unary minus

				double x;
				int startPos = this.pos;
				if (eat('(')) { // parentheses
					x = parseExpression();
					eat(')');
				} else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
					while ((ch >= '0' && ch <= '9') || ch == '.')
						nextChar();
					x = Double.parseDouble(str.substring(startPos, this.pos));
				} else if (ch >= 'a' && ch <= 'z') { // functions
					while (ch >= 'a' && ch <= 'z')
						nextChar();
					String func = str.substring(startPos, this.pos);
					x = parseFactor();
					if (func.equals("sqrt"))
						x = Math.sqrt(x);
					else if (func.equals("sin"))
						x = Math.sin(Math.toRadians(x));
					else if (func.equals("cos"))
						x = Math.cos(Math.toRadians(x));
					else if (func.equals("tan"))
						x = Math.tan(Math.toRadians(x));
					else
						throw new RuntimeException("Unknown function: " + func);
				} else {
					throw new RuntimeException("Unexpected: " + (char) ch);
				}

				if (eat('^'))
					x = Math.pow(x, parseFactor()); // exponentiation

				return x;
			}
		}.parse();
	}
}
