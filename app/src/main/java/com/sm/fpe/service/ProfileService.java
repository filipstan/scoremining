package com.sm.fpe.service;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sm.fpe.dao.ProfileDao;
import com.sm.fpe.model.Competition;
import com.sm.fpe.model.Profile;

@Service
public class ProfileService {

	@Autowired
	private ProfileDao profileDao;

	public Profile save(Profile profile) {
		return profileDao.save(profile);
	}

	public List<Profile> getAllProfiles() {
		return profileDao.findAll();
	}

	public Profile getProfileById(int id) {
		return profileDao.findOne(id);
	}

	@SuppressWarnings("unchecked")
	public String toJSON(List<Profile> profiles) {

		JSONObject profileJSON;
		JSONArray profileCompetitionsJSON, profilesJSON;

		profileJSON = new JSONObject();
		profileCompetitionsJSON = new JSONArray();
		profilesJSON = new JSONArray();

		for (Profile profile : profiles) {
			profileJSON = new JSONObject();
			profileCompetitionsJSON = new JSONArray();
			profileJSON.put("id", profile.getId());
			profileJSON.put("name", profile.getName());
			profileJSON.put("minSup", Integer.toString(profile.getMinSup()));
			profileJSON.put("maxGap", Integer.toString(profile.getMaxGap()));
			profileJSON.put("sequenceMaxLength", Integer.toString(profile.getSequenceMaxLength()));
			profileJSON.put("scoreTypes", profile.getScoreTypes());
			profileJSON.put("strategyType", profile.getStrategyType().toString());
			profileJSON.put("trustFormula", profile.getTrustFormula());
			for (Competition competition : profile.getCompetitions()) {
				profileCompetitionsJSON.add(competition.getName() + " - " + competition.getCountry().getName());
			}
			profileJSON.put("competitions", profileCompetitionsJSON);
			profilesJSON.add(profileJSON);
		}

		return profilesJSON.toJSONString();
	}

	public void removeProfile(int id) {

		profileDao.delete(id);
	}
}
