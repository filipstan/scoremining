//package com.sm.fpe;
//
//import java.sql.Timestamp;
//
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
//
//import com.sm.fpe.dao.TeamDao;
//import com.sm.fpe.service.DataUtils;
//import com.sm.fpe.service.PredictionService;
//import com.sm.fpe.service.SequenceMiningService;
//
//public class Main {
//
//	public static void main(String[] argv) {
//
//		// Item<Integer> i1 = new Item<Integer>(1);
//		// Item<Integer> i2 = new Item<Integer>(2);
//		// Item<Integer> i3 = new Item<Integer>(3);
//		// Item<Integer> i4 = new Item<Integer>(4);
//		//
//		// Transaction<Integer> t1 = new Transaction<Integer>(Arrays.asList(i1,
//		// i4), new Timestamp(0));
//		// Transaction<Integer> t2 = new Transaction<Integer>(Arrays.asList(i4,
//		// i1, i2), new Timestamp(1));
//		// Transaction<Integer> t3 = new Transaction<Integer>(Arrays.asList(i2,
//		// i1, i4, i3), new Timestamp(0));
//		//
//		// Sequence<Integer> s = new Sequence<Integer>(Arrays.asList(t1, t2,
//		// t3));
//		//
//		// System.out.println(s);
//		//
//		// s.removeTransactionsAfterTimestamp(new Timestamp(1));
//		//
//		// System.out.println(s);
//
//		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-servlet.xml");
//
//		Timestamp start, end;
//		DataUtils dataUtils = (DataUtils) context.getBean("dataUtils");
//		SequenceMiningService smService = (SequenceMiningService) context.getBean("sequenceMiningService");
//		TeamDao teamDao = (TeamDao) context.getBean("teamDao");
//		PredictionService predictionService = (PredictionService) context.getBean("predictionService");
//		ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) context.getBean("taskExecutor");
//
////		 dataUtils.saveAllFilesToDb();
//
////		 dataUtils.classifyTeams();
//
////		 dataUtils.mergeTeams();
//
//		// List<Sequence<Integer>> sequences =
//		// smService.getSequencesByTeam(teamDao.findOne(1),
//		// ScoreType.HomeScoreCurrentTeam);
//		// System.out.println(ScoreType.HomeScoreCurrentTeam.name() + " " +
//		// sequences);
//
//		// System.out.println();
//
//		// smService.setMinSup(0.5);
//		// smService.runPrefixSpan(sequences);
//		// smService.printResPrefixes();
//
//		// dataUtils.convertMatchPlayedToUnplayed(30210);
//
//		start = Timestamp.valueOf("2017-05-04 00:00:00.0");
//		end = Timestamp.valueOf("2017-05-04 23:59:59.999");
//
////		dataUtils.convertMatchesToUnplayed(start, end);
//
////		predictionService.computePredictions();
//		predictionService.getPredictions();
//
//		taskExecutor.shutdown();
//
//		context.close();
//	}
//}
