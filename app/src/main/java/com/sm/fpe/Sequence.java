package com.sm.fpe;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class Sequence<T extends Comparable<T>> {
	private List<Transaction<T>> list;
	private int frequency;

	public Sequence() {
		super();
		this.list = new ArrayList<>();
	}

	public Sequence(List<Transaction<T>> list) {
		super();
		this.list = list;
		Collections.sort(this.list);
	}

	public List<Transaction<T>> getList() {
		return list;
	}

	public void setList(List<Transaction<T>> list) {
		this.list = list;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public T getLastItemValue() {

		if (this.list == null || this.list.size() == 0) {
			throw new RuntimeException("Empty or null sequence.");
		}

		Transaction<T> lastTransaction = this.list.get(this.list.size() - 1);
		Item<T> lastItem = lastTransaction.getList().get(lastTransaction.getList().size() - 1);

		return lastItem.getValue();
	}

	public void add(Transaction<T> transaction) {
		this.list.add(transaction);
	}

	// add a transaction with given item
	public void add(Item<T> item, Timestamp date) {
		this.list.add(new Transaction<>(item, date));
	}

	public Sequence<T> copySequence() {
		Sequence<T> retSeq = new Sequence<>();
		Item<T> newItem;
		Transaction<T> newTransaction = null;

		for (Transaction<T> _transaction : this.getList()) {
			newTransaction = new Transaction<>(_transaction.getDate());
			for (Item<T> _item : _transaction.getList()) {
				newItem = new Item<T>(_item.getValue());
				newTransaction.add(newItem);
			}
			retSeq.add(newTransaction);
		}

		return retSeq;
	}

	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	public void orderSequence() {
		Collections.sort(this.list);
	}

	public void removeTransactionsAfterTimestamp(Timestamp timestamp) {
		this.list = this.list.stream().filter(_transaction -> {
			return _transaction.getDate().compareTo(timestamp) < 0;
		}).collect(Collectors.toList());
	}

	public void keepLastTransactions(int sequenceMaxLength) {

		int fromIndex = 0;

		if (sequenceMaxLength > 0 && sequenceMaxLength < this.list.size()) {
			fromIndex = this.list.size() - sequenceMaxLength;
		}

		this.list = this.list.subList(fromIndex, this.list.size());
	}

	public String printPrefix() {
		return this.toString() + "[" + this.frequency + "] ";
	}

	public Sequence<T> getSequenceAfterItem(Item<T> item) {

		Sequence<T> retSeq = new Sequence<>();
		Item<T> newItem;
		Transaction<T> newTransaction = null;
		boolean found = false;

		for (Transaction<T> _transaction : this.getList()) {
			newTransaction = new Transaction<>(_transaction.getDate());
			for (Item<T> _item : _transaction.getList()) {
				if (!found) {
					if (_item.equals(item)) {
						found = true;
					}
				} else {
					newItem = new Item<T>(_item.getValue());
					newTransaction.add(newItem);
				}
			}

			if (!newTransaction.isEmpty()) {
				retSeq.add(newTransaction);
			} else {
				newTransaction = null;
			}
		}

		return retSeq;

	}
	
	public Timestamp getStartTimestamp() {
		
		if (this.getList().size() > 0) {
			return this.getList().get(0).getDate();
		}
		
		return null;
	}

	private HashMap<Item<T>, List<Timestamp>> getHashMapListTT() {

		HashMap<Item<T>, List<Timestamp>> hm = new HashMap<>();
		List<Timestamp> list;

		for (Transaction<T> transaction : this.getList()) {
			for (Item<T> item : transaction.getList()) {
				if (hm.get(item) != null) {
					hm.get(item).add(transaction.getDate());
				} else {
					list = new ArrayList<>();
					list.add(transaction.getDate());
					hm.put(item, list);
				}
			}
		}

		return hm;
	}

	public boolean findSequence(Sequence<T> sequenceToFind, long maxGap) {

		HashMap<Item<T>, List<Timestamp>> hm;
		Timestamp timeThreshold, prevTime, crtTime;
		List<Timestamp> list;
		Transaction<T> transaction;
		Item<T> item;
		int i, j, index, foundIndex;
		boolean back;
		Stack<Timestamp> stack;

		hm = this.getHashMapListTT();
		timeThreshold = new Timestamp(0);
		prevTime = null;
		crtTime = null;
		index = -1;
		foundIndex = -1;
		back = false;
		stack = new Stack<>();

		for (i = 0; i < sequenceToFind.getList().size(); i++) {

			transaction = sequenceToFind.getList().get(i);

			for (j = 0; j < transaction.getList().size(); j++) {

				index++;

				item = transaction.getList().get(j);

				list = hm.get(item);

				if (list == null) {
					return false;
				}

				for (Timestamp timestamp : list) {
					if (timestamp.compareTo(timeThreshold) > 0) {
						crtTime = timestamp;
						foundIndex = index;
						stack.push(crtTime);
						break;
					}
				}

				if (index != foundIndex) {
					return false;
				}

				if (prevTime != null) {
					if (((crtTime.getTime() - prevTime.getTime()) / 1000) > maxGap) {
						back = true;
						index -= 2;
						foundIndex -= 2;
						timeThreshold = new Timestamp((crtTime.getTime() / 1000 - maxGap) * 1000);
						stack.pop();
						stack.pop();
						if (stack.isEmpty() == false) {
							prevTime = stack.peek();
						} else {
							prevTime = null;
						}
						if (j == 0) {
							i -= 2;
						} else {
							j -= 2;
						}
					}
				}

				if (back == false) {
					timeThreshold = crtTime;
					prevTime = crtTime;
				}

				back = false;
			}
		}

		return (foundIndex + 1) == sequenceToFind.size();
	}

	public int size() {

		int size = 0;

		for (Transaction<T> transaction : this.getList()) {
			size += transaction.getList().size();
		}

		return size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sequence<T> other = (Sequence<T>) obj;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (!isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (Transaction<T> item : this.list) {
				sb.append(item + ", ");
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.deleteCharAt(sb.length() - 1);
			return "<" + sb + ">";
			// return "Sequence [list=" + list + "\n\t]";
		} else {
			return "<>";
		}
	}
}
