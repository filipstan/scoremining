package com.sm.fpe.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.json.simple.JSONValue;

@Entity
@Table(name = "NOTIFICATION")
public class Notification implements Serializable {
	private static final long serialVersionUID = -6030869681411806050L;
	private int id;
	private User user;
	private Boolean read;
	private String message;
	private Timestamp timestamp;

	public Notification() {
		super();
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "IS_READ")
	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	@Column(name = "MESSAGE")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "TIMESTAMP")
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((read == null) ? 0 : read.hashCode());
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notification other = (Notification) obj;
		if (id != other.id)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (read == null) {
			if (other.read != null)
				return false;
		} else if (!read.equals(other.read))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", user=" + user + ", read=" + read + ", message=" + message + ", timestamp="
				+ timestamp + "]";
	}

	public String toJSON() {

		DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		StringBuilder sb = new StringBuilder();

		sb.append("{");
		sb.append("\"id\":").append("\"").append(this.getId()).append("\",");
		sb.append("\"read\":").append("\"").append(this.getRead()).append("\",");
		sb.append("\"user\":").append("\"").append(this.getUser().getEmail()).append("\",");
		sb.append("\"message\":").append("\"").append(JSONValue.escape(this.getMessage())).append("\",");
		sb.append("\"timestamp\":").append("\"").append(format.format(this.getTimestamp())).append("\"");
		sb.append("}");

		return sb.toString();

	}

}
