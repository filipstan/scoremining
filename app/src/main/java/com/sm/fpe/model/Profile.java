package com.sm.fpe.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sm.fpe.ScoreType;
import com.sm.fpe.StrategyType;

@Entity
@Table(name = "PROFILE")
public class Profile implements Serializable {
	private static final long serialVersionUID = -7057241027013802645L;
	private int id;
	private String name;
	private int minSup;
	private String scoreTypes;
	private StrategyType strategyType;
	private String trustFormula;
	private int sequenceMaxLength;
	private int maxGap;
	private Collection<Competition> competitions;

	public Profile() {
		super();
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "MIN_SUP")
	public int getMinSup() {
		return minSup;
	}

	public void setMinSup(int minSup) {
		this.minSup = minSup;
	}

	@Column(name = "SCORE_TYPES")
	public String getScoreTypes() {
		return scoreTypes;
	}

	public void setScoreTypes(String scoreTypes) {
		this.scoreTypes = scoreTypes;
	}

	@Transient
	public List<ScoreType> getScoreTypesList() {

		List<ScoreType> scoreTypeList = new ArrayList<>();

		String[] tokens = this.scoreTypes.split(", ");

		for (int i = 0; i < tokens.length; i++) {
			scoreTypeList.add(ScoreType.valueOf(tokens[i]));
		}

		return scoreTypeList;
	}

	public void setScoreTypes(List<ScoreType> scoreTypes) {

		StringBuilder sb = new StringBuilder();

		for (ScoreType scoreType : scoreTypes) {
			sb.append(scoreType + ", ");
		}

		this.scoreTypes = sb.toString();

	}

	@Column(name = "STRATEGY_TYPE")
	public StrategyType getStrategyType() {
		return strategyType;
	}

	public void setStrategyType(StrategyType strategyType) {
		this.strategyType = strategyType;
	}

	@Column(name = "TRUST_FORMULA")
	public String getTrustFormula() {
		return trustFormula;
	}

	public void setTrustFormula(String trustFormula) {
		this.trustFormula = trustFormula;
	}

	@Column(name = "SEQUENCE_MAX_LENGTH")
	public int getSequenceMaxLength() {
		return sequenceMaxLength;
	}

	public void setSequenceMaxLength(int sequenceMaxLength) {
		this.sequenceMaxLength = sequenceMaxLength;
	}

	@Column(name = "MAX_GAP")
	public int getMaxGap() {
		return maxGap;
	}

	public void setMaxGap(int maxGap) {
		this.maxGap = maxGap;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "PROFILES_COMPETITIONS", 
	joinColumns = @javax.persistence.JoinColumn(name = "PROFILE_ID", 
	referencedColumnName = "id"), 
	inverseJoinColumns = @javax.persistence.JoinColumn(name = "COMPETITION_ID", referencedColumnName = "id"))
	public Collection<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(Collection<Competition> competitions) {
		this.competitions = competitions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + maxGap;
		result = prime * result + minSup;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((scoreTypes == null) ? 0 : scoreTypes.hashCode());
		result = prime * result + sequenceMaxLength;
		result = prime * result + ((strategyType == null) ? 0 : strategyType.hashCode());
		result = prime * result + ((trustFormula == null) ? 0 : trustFormula.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (id != other.id)
			return false;
		if (maxGap != other.maxGap)
			return false;
		if (minSup != other.minSup)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (scoreTypes == null) {
			if (other.scoreTypes != null)
				return false;
		} else if (!scoreTypes.equals(other.scoreTypes))
			return false;
		if (sequenceMaxLength != other.sequenceMaxLength)
			return false;
		if (strategyType != other.strategyType)
			return false;
		if (trustFormula == null) {
			if (other.trustFormula != null)
				return false;
		} else if (!trustFormula.equals(other.trustFormula))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", name=" + name + ", minSup=" + minSup + ", scoreTypes=" + scoreTypes
				+ ", strategyType=" + strategyType + ", trustFormula=" + trustFormula + ", sequenceMaxLength="
				+ sequenceMaxLength + ", maxGap=" + maxGap + "]";
	}

}
