package com.sm.fpe.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sm.fpe.ScoreType;

@Entity
@Table(name = "MATCH_PREDICTION")
public class MatchPrediction implements Serializable {
	private static final long serialVersionUID = 6341319836075831590L;
	private int id;
	private int score;
	private ScoreType scoreType;
	private MatchUnplayed matchUnplayed;
	private int trust;
	private Profile profile;

	public MatchPrediction() {
		super();
	}

	public MatchPrediction(MatchUnplayed matchUnplayed, int score, ScoreType scoreType, int trust, Profile profile) {
		super();
		this.score = score;
		this.scoreType = scoreType;
		this.matchUnplayed = matchUnplayed;
		this.trust = trust;
		this.profile = profile;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "SCORE")
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Column(name = "SCORE_TYPE")
	public ScoreType getScoreType() {
		return scoreType;
	}

	public void setScoreType(ScoreType scoreType) {
		this.scoreType = scoreType;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "MATCH_UNPLAYED_ID")
	public MatchUnplayed getMatchUnplayed() {
		return matchUnplayed;
	}

	public void setMatchUnplayed(MatchUnplayed matchUnplayed) {
		this.matchUnplayed = matchUnplayed;
	}

	@Column(name = "TRUST")
	public int getTrust() {
		return trust;
	}

	public void setTrust(int trust) {
		this.trust = trust;
	}

	@ManyToOne
	@JoinColumn(name = "PROFILE_ID")
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public static int compareMatchScoreWithPrediction(int homeScore, int awayScore, int homeScorePrediction,
			int awayScorePrediction) {
		if (homeScore == homeScorePrediction && awayScore == awayScorePrediction) {
			return 100;
		}

		if (Math.signum(homeScore - awayScore) == Math.signum(homeScorePrediction - awayScorePrediction)) {
			return 50;
		}

		if (homeScore == homeScorePrediction || awayScore == awayScorePrediction) {
			return 25;
		}

		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matchUnplayed == null) ? 0 : matchUnplayed.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + score;
		result = prime * result + ((scoreType == null) ? 0 : scoreType.hashCode());
		result = prime * result + trust;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchPrediction other = (MatchPrediction) obj;
		if (matchUnplayed == null) {
			if (other.matchUnplayed != null)
				return false;
		} else if (!matchUnplayed.equals(other.matchUnplayed))
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (score != other.score)
			return false;
		if (scoreType != other.scoreType)
			return false;
		if (trust != other.trust)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MatchPrediction [id=" + id + ", score=" + score + ", scoreType=" + scoreType + ", matchUnplayed="
				+ matchUnplayed + ", trust=" + trust + ", profile=" + profile + "]";
	}

}
