package com.sm.fpe.model;

public enum CompetitionLevel {
	LEVEL1(1),
	LEVEL2(2),
	LEVEL3(3),
	LEVEL4(4),
	LEVEL5(5);
	
	private final int id;
	
	CompetitionLevel(int id){
		this.id = id;
	}
}