package com.sm.fpe.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "MATCH_UNPLAYED")
public class MatchUnplayed implements Serializable {
	private static final long serialVersionUID = -8141655403756753859L;
	private int id;
	private Team homeTeam;
	private Team awayTeam;
	private Competition competition;
	private Timestamp timestamp;
	
	private Map<Integer, Integer> homeScorePredictions;
	private Map<Integer, Integer> awayScorePredictions;

	public MatchUnplayed() {
		super();
	}
	
	public MatchUnplayed(Match match) {
		super();
		this.homeTeam = match.getHomeTeam();
		this.awayTeam = match.getAwayTeam();
		this.competition = match.getCompetition();
		this.timestamp = match.getTimestamp();
	}

	public MatchUnplayed(Team homeTeam, Team awayTeam, Competition competition, Timestamp timestamp) {
		super();
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.competition = competition;
		this.timestamp = timestamp;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "HOME_TEAM_ID")
	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "AWAY_TEAM_ID")
	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "COMPETITION_ID")
	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	@Column(name = "DATE")
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	@Transient
	public Map<Integer, Integer> getHomeScorePredictions() {
		return homeScorePredictions;
	}

	public void setHomeScorePredictions(Map<Integer, Integer> homeScorePredictions) {
		this.homeScorePredictions = homeScorePredictions;
	}

	@Transient
	public Map<Integer, Integer> getAwayScorePredictions() {
		return awayScorePredictions;
	}

	public void setAwayScorePredictions(Map<Integer, Integer> awayScorePredictions) {
		this.awayScorePredictions = awayScorePredictions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((awayTeam == null) ? 0 : awayTeam.hashCode());
		result = prime * result + ((competition == null) ? 0 : competition.hashCode());
		result = prime * result + ((homeTeam == null) ? 0 : homeTeam.hashCode());
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchUnplayed other = (MatchUnplayed) obj;
		if (awayTeam == null) {
			if (other.awayTeam != null)
				return false;
		} else if (!awayTeam.equals(other.awayTeam))
			return false;
		if (competition == null) {
			if (other.competition != null)
				return false;
		} else if (!competition.equals(other.competition))
			return false;
		if (homeTeam == null) {
			if (other.homeTeam != null)
				return false;
		} else if (!homeTeam.equals(other.homeTeam))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MatchUnplayed [id=" + id + ", homeTeam=" + homeTeam + ", awayTeam=" + awayTeam + ", competition="
				+ competition + ", timestamp=" + timestamp + "]";
	}

}
