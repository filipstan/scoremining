package com.sm.fpe.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "MATCH")
public class Match implements Serializable {
	private static final long serialVersionUID = -3151840173707353677L;
	private int id;
	private Team homeTeam;
	private Team awayTeam;
	private Competition competition;
	private int homeScore;
	private int awayScore;
	private Timestamp timestamp;

	public Match() {
		super();
	}

	public Match(Team homeTeam, Team awayTeam, Competition competition, int homeScore, int awayScore,
			Timestamp timestamp) {
		super();
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
		this.competition = competition;
		this.homeScore = homeScore;
		this.awayScore = awayScore;
		this.timestamp = timestamp;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "HOME_TEAM_ID")
	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "AWAY_TEAM_ID")
	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "COMPETITION_ID")
	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	@Column(name = "HOME_SCORE")
	public int getHomeScore() {
		return homeScore;
	}

	public void setHomeScore(int homeScore) {
		this.homeScore = homeScore;
	}

	@Column(name = "AWAY_SCORE")
	public int getAwayScore() {
		return awayScore;
	}

	public void setAwayScore(int awayScore) {
		this.awayScore = awayScore;
	}

	@Column(name = "DATE")
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	@Transient
	public double getHomePoints() {
		int scoreDif = homeScore - awayScore;

		if (scoreDif > 0) {
			return 3;
		} else if (scoreDif == 0) {
			return 1;
		} else {
			return 0;
		}
	}

	@Transient
	public double getAwayPoints() {
		int scoreDif = awayScore - homeScore;

		if (scoreDif > 0) {
			return 3.5;
		} else if (scoreDif == 0) {
			return 1.5;
		} else {
			return 0;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + awayScore;
		result = prime * result + ((awayTeam == null) ? 0 : awayTeam.hashCode());
		result = prime * result + ((competition == null) ? 0 : competition.hashCode());
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		result = prime * result + homeScore;
		result = prime * result + ((homeTeam == null) ? 0 : homeTeam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Match other = (Match) obj;
		if (awayScore != other.awayScore)
			return false;
		if (awayTeam == null) {
			if (other.awayTeam != null)
				return false;
		} else if (!awayTeam.equals(other.awayTeam))
			return false;
		if (competition == null) {
			if (other.competition != null)
				return false;
		} else if (!competition.equals(other.competition))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (homeScore != other.homeScore)
			return false;
		if (homeTeam == null) {
			if (other.homeTeam != null)
				return false;
		} else if (!homeTeam.equals(other.homeTeam))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Match [id=" + id + ", homeTeam=" + homeTeam + ", awayTeam=" + awayTeam + ", competition=" + competition
				+ ", homeScore=" + homeScore + ", awayScore=" + awayScore + ", date=" + timestamp + "]";
	}
}
