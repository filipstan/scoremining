package com.sm.fpe.model;

public enum CompetitionType {
	NATIONAL_WORLDWIDE(1),
	NATIONAL_INTERCONTINENTAL(2),
	NATIONAL_YOUTH(3),
	CLUBS_WORLDWIDE(4),
	CLUBS_CONTINENTAL(5),
	CLUBS_NATIONAL_LEAGUE(6);
	
	private final int id;
	
	CompetitionType(int id){
		this.id = id;
	}
}