package com.sm.fpe.model;

public enum TeamLevel {
	LEVEL1(1),
	LEVEL2(2),
	LEVEL3(3),
	LEVEL4(4),
	LEVEL5(5),
	LEVEL6(6),
	LEVEL7(7),
	LEVEL8(8),
	LEVEL9(9),
	LEVEL10(10),
	LEVEL11(11),
	LEVEL12(12),
	LEVEL13(13),
	LEVEL14(14),
	LEVEL15(15),
	LEVEL16(16),
	LEVEL17(17),
	LEVEL18(18),
	LEVEL19(19),
	LEVEL20(20);
	
	private final int id;
	
	TeamLevel(int id){
		this.id = id;
	}
	
	public static TeamLevel getTeamLevel(int id){
		if (id <= 1) return LEVEL1;
		else if (id <= 2) return LEVEL2;
		else if (id <= 3) return LEVEL3;
		else if (id <= 4) return LEVEL4;
		else if (id <= 5) return LEVEL5;
		else if (id <= 6) return LEVEL6;
		else if (id <= 7) return LEVEL7;
		else if (id <= 8) return LEVEL8;
		else if (id <= 9) return LEVEL9;
		else if (id <= 10) return LEVEL10;
		else if (id <= 11) return LEVEL11;
		else if (id <= 12) return LEVEL12;
		else if (id <= 13) return LEVEL13;
		else if (id <= 14) return LEVEL14;
		else if (id <= 15) return LEVEL15;
		else if (id <= 16) return LEVEL16;
		else if (id <= 17) return LEVEL17;
		else if (id <= 18) return LEVEL18;
		else if (id <= 19) return LEVEL19;
		else return LEVEL20;
	}
}