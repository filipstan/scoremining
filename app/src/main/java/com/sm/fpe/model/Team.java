package com.sm.fpe.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Entity
@Table(name = "TEAM")
public class Team implements Serializable {
	private static final long serialVersionUID = 6403855547181609294L;
	private int id;
	private String name;
	private TeamLevel level;
	private Country country;
	private List<Match> homeMatches;
	private List<Match> awayMatches;

	public Team() {
		super();
	}

	public Team(String name, Country country) {
		super();
		this.name = name.trim().replaceAll("\\(.{3}\\)$", "").trim();
		this.country = country;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "LEVEL")
	public TeamLevel getLevel() {
		return level;
	}

	public void setLevel(TeamLevel level) {
		this.level = level;
	}

	@ManyToOne
	@JoinColumn(name = "COUNTRY_ID")
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "homeTeam")
	public List<Match> getHomeMatches() {
		return homeMatches;
	}

	public void setHomeMatches(List<Match> homeMatches) {
		this.homeMatches = homeMatches;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true, noRollbackFor = Exception.class)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "awayTeam")
	public List<Match> getAwayMatches() {
		return awayMatches;
	}

	public void setAwayMatches(List<Match> awayMatches) {
		this.awayMatches = awayMatches;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + id;
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (id != other.id)
			return false;
		if (level != other.level)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", level=" + level + ", country=" + country + "]";
	}
}
